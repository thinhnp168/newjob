$('.main-slide').owlCarousel({
    loop: true,
    items: 1,
    autoplay: true,
    nav: false
})
$('.t-report-slide').owlCarousel({
    loop: true,
    items: 1,
    autoplay: true,
    dots: false,
    nav: true,
    navText: ["<img src='/client/image/nav-carousel-left.png'>", "<img src='/client/image/nav-carousel.png''>"]
})
$('.small-slide').owlCarousel({
    loop: true,
    items: 3,
    autoplay: true,
    margin: 10,
    dots: true,
    nav: false
})
$('.doitac-slide').owlCarousel({
    loop: true,
    autoplay: true,
    margin: 10,
    dots: false,
    nav: false,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 6
        }
    }
})
$(document).ready(function() {
    $('.t-fa-star').mouseover(function() {
        setStarcolor();
        var curent = parseInt($(this).data('value'));
        for (var i = 0; i < curent; i++) {
            $('.t-fa-star:eq(' + i + ')').css('color', '#FF9800');
        }

    });
    var index = -1;
    $('.t-fa-star').on('click', function() {
        index = parseInt($(this).data('value'));
        $('.danhgia').val(index);
    });
    $('.t-fa-star').mouseleave(function() {
        setStarcolor();
        for (var i = 0; i < index; i++) {
            $('.t-fa-star:eq(' + i + ')').css('color', '#FF9800');
        }
    });
});

function setStarcolor() {
    $('.t-fa-star').css('color', '#ccc');
}