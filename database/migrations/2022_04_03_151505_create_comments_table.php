<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('cmt_name')->index();
            $table->string('cmt_email')->index()->nullable();
            $table->longText('cmt_content');
            $table->tinyInteger('cmt_status')->default(0)->comment('0: block, 1: active');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->bigInteger('post_id')->unsigned()->nullable();
            $table->bigInteger('parent_id')->nullable();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}