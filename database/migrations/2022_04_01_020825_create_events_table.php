<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('eve_name')->index();
            $table->string('eve_slug');
            $table->string('eve_image');
            $table->string('eve_avatar');
            $table->text('eve_description');
            $table->date('eve_start_date');
            $table->date('eve_end_date');
            $table->string('eve_end_hour')->nullable();
            $table->string('eve_start_hour')->nullable();
            $table->integer('eve_price')->default(0);
            $table->tinyInteger('eve_address_type')->default(0)->comment('0: offline, 1: online');
            $table->string('eve_address')->nullable();
            $table->integer('eve_love')->default(0);
            $table->longText('eve_introduce')->nullable();
            $table->longText('eve_content')->nullable();
            $table->longText('eve_lecturer')->nullable();
            $table->tinyInteger('eve_is_hot')->default(0)->comment('0: no-hot, 1: hot');
            $table->tinyInteger('eve_status')->default(0)->comment('0: draft, 1: approved, 2: not-approved');
            $table->string('eve_seo_title');
            $table->text('eve_seo_description');
            $table->text('eve_seo_keyword');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
