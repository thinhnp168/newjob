<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('pts_name')->index();
            $table->mediumText('pts_image');
            $table->string('pts_seo_title');
            $table->text('pts_description');
            $table->text('pts_seo_description');
            $table->text('pts_seo_keyword');
            $table->string('pts_slug');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->longText('pts_content');
            $table->tinyInteger('pts_status')->default(1)->comment('0: block, 1: active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
