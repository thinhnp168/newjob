<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->id();
            $table->string('con_name');
            $table->string('con_website');
            $table->string('con_email');
            $table->string('con_tagline');
            $table->mediumText('con_facebook');
            $table->mediumText('con_youtube');
            $table->string('con_zalo');
            $table->mediumText('con_adress_hn');
            $table->mediumText('con_adress_hcm');
            $table->mediumText('con_adress');
            $table->string('con_phone_hn');
            $table->string('con_phone_hcm');
            $table->mediumText('con_map');
            $table->mediumText('con_favicon');
            $table->mediumText('con_logo_top');
            $table->mediumText('con_logo_top_small');
            $table->mediumText('con_logo_bot');
            $table->string('con_seo_title');
            $table->text('con_seo_description');
            $table->text('con_seo_keyword');
            $table->text('con_seo_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
