<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'urs_name' => Str::random(10),
            'urs_email' => 'sukienso.com.vn@gmail.com',
            'urs_status' => 1,
            'urs_phone' => '0979718691',
            'password' => Hash::make('12345678'),
        ]);
    }
}
