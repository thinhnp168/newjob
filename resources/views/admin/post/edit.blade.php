@extends('admin.layout.index')
@section('title_site', 'Chỉnh sửa tin tức')
@section('content')
<script src='/client/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.t-mce",
    height: 200,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
    </script>
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Chỉnh sửa tin tức</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        
        <form action="" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Tên:</label>
                        <div class="">
                            <input type="text" class="form-control" name="pts_name" placeholder="Nhập tên" value="{{ $editPost->pts_name }}">
                            @error('pts_name')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Xuất bản:</label>
                        <div class="">
                            <select name="pts_status" id="" class="form-control" >
                                <option value="1" {{ $editPost->pts_status == 1 ? 'selected' : "" }} >Hiển thị</option>
                                <option value="0" {{ $editPost->pts_status == 0 ? 'selected' : "" }} >Ẩn</option>
                            </select>
                            @error('pts_status')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Mô tả ngắn:</label>
                        <div class="">
                            <textarea name="pts_description" class="form-control" rows="3">{!! $editPost->pts_description !!}</textarea>
                            @error('pts_description')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Ảnh bìa :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-pts_image" data-input="pts_image" data-preview="pts_image-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh bìa
                            </a>
                            </span>
                            <input id="pts_image" class="form-control" type="text" name="pts_image" value="{{ $editPost->pts_image }}">
                        </div>
                        @error('pts_image')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="pts_image-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $editPost->pts_image }}" alt="" style="height: 5rem;">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Title:</label>
                        <div class="">
                            <input type="text" class="form-control" name="pts_seo_title" placeholder="Nhập title" value="{{ $editPost->pts_seo_keyword }}">
                            @error('pts_seo_title')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Keyword:</label>
                        <div class="">
                            <input type="text" class="form-control" name="pts_seo_keyword" placeholder="Nhập keyword" value="{{ $editPost->pts_seo_keyword }}">
                            @error('pts_seo_keyword')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Description:</label>
                        <div class="">
                            <textarea name="pts_seo_description" id="" class="form-control" rows="3">{!! $editPost->pts_seo_description !!}</textarea>
                            @error('pts_seo_description')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                </div>
            </div>
            
            <div class="form-group">
                <label class="col-form-label" for="teacher">Chi tiết tin tức</label>
                <textarea name="pts_content" class="form-control teacher t-mce" id="content" >{!! $editPost->pts_content !!}</textarea>
                @error('pts_content')
                    <span class="form-text text-danger">{{ $message }}</span>
                @enderror
            </div>

           

            <div class="form-group row mb-0">
                <div class="col-12 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@if(session()->has('success_mess'))
		<script>
			let messages = '{{ session()->get('success_mess') }}';
			toastr.success(messages);
		</script>
	@endif
	@if(session()->has('error_mess'))
		<script>
			let mess = '{{ session()->get('error_mess') }}';
			toastr.error(mess);
		</script>
	@endif

@endsection
@push('scripts')
<script>
  
</script>
@endpush
@push('theme-js')
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('#t-lfm-pts_image').filemanager('image');
    </script>
@endpush

