@extends('admin.layout.index')
@section('title_site', 'Danh sách Số điện thoại đăng ký')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Danh sách Số điện thoại đăng ký</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <table class="table datatable-basic table-bordered t-datatable-basic">
        <thead>
            <tr>
                <th>ID</th>
                <th>Phone</th>
                <th class="d-none">Phone</th>
                <th class="d-none">Phone</th>
                <th class="d-none">Phone</th>
                <th class="d-none">Phone</th>
                {{-- <th class="text-center">Actions</th> --}}
            </tr>
        </thead>
        <tbody>
            @foreach ($listPhones as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->pho_phone }}</td>
                    <td class="d-none">{{ $item->pho_phone }}</td>
                    <td class="d-none">{{ $item->pho_phone }}</td>
                    <td class="d-none">{{ $item->pho_phone }}</td>
                    <td class="d-none">{{ $item->pho_phone }}</td>
                    
                    {{-- <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="" class="dropdown-item"><i class="icon-database-edit2"></i> Sửa</a>
                                    <a href="" class="dropdown-item" onclick="return confirm('Bạn chắc chắn muốn xoá?')"><i class="icon-database-remove" ></i> Xoá</a>
                                </div>
                            </div>
                        </div>
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@push('scripts')
    
@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/back/global_assets/js/demo_pages/datatables_basic.js"></script>
@endpush

