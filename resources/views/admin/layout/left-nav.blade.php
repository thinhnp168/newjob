<ul class="nav nav-sidebar" data-nav-type="accordion">
    
    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
    
    <li class="nav-item">
        <a href="index.html" class="nav-link active">
            <i class="icon-home4"></i>
            <span>
                Dashboard
                <span class="d-block font-weight-normal opacity-50">No active orders</span>
            </span>
        </a>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Người dùng</span></a>
        
        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-user') }}" class="nav-link">Danh sách</a></li>
            <li class="nav-item"><a href="{{ route('form-add-user') }}" class="nav-link">Thêm mới</a></li>
        </ul>
    </li>
    @role('Super Admin')
        <li class="nav-item nav-item-submenu">
            <a href="" class="nav-link"><i class="icon-user"></i> <span>Permission</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="">
                <li class="nav-item"><a href="{{ route('list-role') }}" class="nav-link">Danh sách vai trò</a></li>
                <li class="nav-item"><a href="{{ route('form-add-role') }}" class="nav-link">Thêm mới vai trò (role)</a></li>
                <li class="nav-item"><a href="{{ route('list-permission') }}" class="nav-link">Danh sách quyền</a></li>
                <li class="nav-item"><a href="{{ route('form-add-permission') }}" class="nav-link">Thêm mới quyền (permission)</a></li>
            </ul>
        </li>
    @endrole
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Tin tức</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-post') }}" class="nav-link">Danh sách</a></li>
            <li class="nav-item"><a href="{{ route('form-add-post') }}" class="nav-link">Thêm mới</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Sự kiện</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-event') }}" class="nav-link">Danh sách</a></li>
            <li class="nav-item"><a href="{{ route('form-add-event') }}" class="nav-link">Thêm mới</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Số điện thoại đăng ký</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-phone') }}" class="nav-link">Danh sách</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Liên hệ</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-contact') }}" class="nav-link">Danh sách</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Comment Post</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('list-comment') }}" class="nav-link">Danh sách</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Config</span></a>
        
        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('form-add-config') }}" class="nav-link">Chỉnh sửa</a></li>
        </ul>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="" class="nav-link"><i class="icon-user"></i> <span>Slider</span></a>
    
        <ul class="nav nav-group-sub" data-submenu-title="">
            <li class="nav-item"><a href="{{ route('form-add-position') }}" class="nav-link">Thêm mới vị trí </a></li>
            <li class="nav-item"><a href="{{ route('list-position') }}" class="nav-link">Danh sách vị trí</a></li>
            <li class="nav-item"><a href="{{ route('form-add-slider') }}" class="nav-link">Thêm mới slider </a></li>
            <li class="nav-item"><a href="{{ route('list-slider') }}" class="nav-link">Danh sách slider </a></li>
        </ul>
    </li>
</ul>