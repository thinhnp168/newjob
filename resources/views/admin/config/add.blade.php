@extends('admin.layout.index')
@section('title_site', 'Quản lý config')
@section('content')

<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Quản lý config</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        
        <form action="" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Tên công ty:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_name"  value="{{ $config ? $config->con_name : '' }}">
                            @error('con_name')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Website:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_website"  value="{{ $config ? $config->con_website : '' }}">
                            @error('con_website')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Email:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_email"  value="{{ $config ? $config->con_email : '' }}">
                            @error('con_email')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Tagline:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_tagline" value="{{ $config ? $config->con_tagline : '' }}" >
                            @error('con_tagline')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Facebook :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_facebook" value="{{ $config ? $config->con_facebook : '' }}">
                            @error('con_facebook')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Zalo :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_zalo" value="{{ $config ? $config->con_zalo : '' }}">
                            @error('con_zalo')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Youtube :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_youtube"  value="{{ $config ? $config->con_youtube : '' }}">
                            @error('con_youtube')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Địa chỉ văn phòng Hà Nội :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_adress_hn" value="{{ $config ? $config->con_adress_hn : '' }}" >
                            @error('con_adress_hn')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Số điện thoại văn phòng Hà Nội :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_phone_hn" value="{{ $config ? $config->con_phone_hn : '' }}" >
                            @error('con_phone_hn')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Địa chỉ văn phòng Hồ Chí Minh :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_adress_hcm"  value="{{ $config ? $config->con_adress_hcm : '' }}">
                            @error('con_adress_hcm')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Số điện thoại văn phòng Hồ Chí Minh :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_phone_hcm" value="{{ $config ? $config->con_phone_hcm : '' }}" >
                            @error('con_phone_hcm')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Địa chỉ trụ sở chính :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_adress"  value="{{ $config ? $config->con_adress : '' }}">
                            @error('con_adress')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Bản đồ :</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_map" value="{{ $config ? $config->con_map : '' }}" >
                            @error('con_map')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Logo top :</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-con_logo_top" data-input="con_logo_top" data-preview="con_logo_top-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh
                            </a>
                            </span>
                            <input id="con_logo_top" class="form-control" type="text" name="con_logo_top" value="{{ $config ? $config->con_logo_top : '' }}">
                        </div>
                        @error('con_logo_top')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="con_logo_top-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $config ?  $config->con_logo_top : '' }}" alt="" style="height: 5rem;">
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Logo top (nhỏ) :</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-con_logo_top_small" data-input="con_logo_top_small" data-preview="con_logo_top_small-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh
                            </a>
                            </span>
                            <input id="con_logo_top_small" class="form-control" type="text" name="con_logo_top_small" value="{{ $config ? $config->con_logo_top_small : '' }}">
                        </div>
                        @error('con_logo_top_small')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="con_logo_top_small-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $config ?  $config->con_logo_top_small : '' }}" alt="" style="height: 5rem;">
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Logo bottom :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-con_logo_bot" data-input="con_logo_bot" data-preview="con_logo_bot-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh
                            </a>
                            </span>
                            <input id="con_logo_bot" class="form-control" type="text" name="con_logo_bot" value="{{ $config ? $config->con_logo_bot : '' }}">
                        </div>
                        @error('con_logo_bot')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="con_logo_bot-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $config ?  $config->con_logo_bot : '' }}" alt="" style="height: 5rem;">
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Favicon :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-con_favicon" data-input="con_favicon" data-preview="con_favicon-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh
                            </a>
                            </span>
                            <input id="con_favicon" class="form-control" type="text" name="con_favicon" value="{{ $config ? $config->con_favicon : '' }}">
                        </div>
                        @error('con_favicon')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="con_favicon-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $config ?  $config->con_favicon : '' }}" alt="" style="height: 5rem;">
                    </div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Title Seo:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_seo_title" value="{{ $config ? $config->con_seo_title : '' }}">
                            @error('con_seo_title')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Keyword Seo:</label>
                        <div class="">
                            <input type="text" class="form-control" name="con_seo_keyword" value="{{ $config ? $config->con_seo_keyword : '' }}" >
                            @error('con_seo_keyword')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Description Seo:</label>
                        <div class="">
                            <textarea name="con_seo_description" id="" class="form-control" rows="3">{!! $config ? $config->con_seo_description : '' !!}</textarea>
                            @error('con_seo_description')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Image Seo :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-con_seo_image" data-input="con_seo_image" data-preview="con_seo_image-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh
                            </a>
                            </span>
                            <input id="con_seo_image" class="form-control" type="text" name="con_seo_image" value="{{ $config ? $config->con_seo_image : '' }}">
                        </div>
                        @error('con_seo_image')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="con_seo_image-holder"  class="d-flex justify-content-center mt-3 mb-3">
                        <img src="{{ $config ?  $config->con_seo_image : '' }}" alt="" style="height: 5rem;">
                    </div>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <div class="col-12 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@if(session()->has('success_mess'))
		<script>
			let messages = '{{ session()->get('success_mess') }}';
			toastr.success(messages);
		</script>
	@endif
	@if(session()->has('error_mess'))
		<script>
			let mess = '{{ session()->get('error_mess') }}';
			toastr.error(mess);
		</script>
	@endif

@endsection
@push('scripts')

@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    {{-- <script src="/back/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/mousewheel.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/globalize.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.de-DE.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.vi-VN.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js"></script> --}}
    <script src="/back/global_assets/js/demo_pages/jqueryui_forms.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('#t-lfm-con_logo_top').filemanager('image');
        $('#t-lfm-con_logo_bot').filemanager('image');
        $('#t-lfm-con_favicon').filemanager('image');
        $('#t-lfm-con_seo_image').filemanager('image');
        $('#t-lfm-con_logo_top_small').filemanager('image');
    </script>
@endpush

