@extends('admin.layout.index')
@section('title_site', 'Thêm mới người dùng')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Thêm mới người dùng</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="" method="post">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tên:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="urs_name" placeholder="Nhập tên" >
                    @error('urs_name')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Email:</label>
                <div class="col-lg-10"> 
                    <input type="email" class="form-control" name="urs_email" placeholder="Nhập email" >
                    @error('urs_email')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Số điện thoại:</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="urs_phone" placeholder="Nhập số điện thoại" >
                    @error('urs_phone')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Mật khẩu:</label>
                <div class="col-lg-10">
                    <input type="password" class="form-control" name="password" placeholder="*****" >
                    @error('password')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-lg-10 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@push('scripts')
    
@endpush
@push('theme-js')
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
@endpush

