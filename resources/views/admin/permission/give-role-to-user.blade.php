@extends('admin.layout.index')
@section('title_site', 'Gán vai trò cho người dùng')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Gán vai trò cho người dùng</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="" method="post">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2 font-weight-bold  ">{{ $user->urs_name }}:</label>
                <div class="col-lg-10">
                    <div class="row">
                        
                        @foreach ($allRole as $item)
                            <div class="col-4 col-md-3">
                                <div class="form-check form-check-switchery">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="role[]" value="{{ $item->name }}" class="form-check-input-switchery" {{ in_array($item->name, $roleName) ? "checked" : '' }}    data-fouc>
                                        {{ $item->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-lg-10 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@push('scripts')
    
@endpush
@push('theme-js')
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/switch.min.js"></script>
<script src="/back/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endpush

