@extends('admin.layout.index')
@section('title_site', 'Thêm mới vai trò')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Thêm mới vai trò</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="" method="post">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tên:</label>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="name" placeholder="Nhập tên vai trò" >
                    @error('name')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-lg-10 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@push('scripts')
    
@endpush
@push('theme-js')
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
@endpush

