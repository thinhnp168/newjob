@extends('admin.layout.index')
@section('title_site', 'Danh sách slider')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Danh sách slider</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
                {{-- <a class="list-icons-item" data-action="remove"></a> --}}
            </div>
        </div>
    </div>

    <table class="table datatable-basic table-bordered t-datatable-basic">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Link</th>
                <th>Ảnh</th>
                <th>Vị trí</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listSliders as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->sli_title }}</td>
                    <td>{{ $item->sli_link }}</td>
                    <td>
                        <img src="{{ $item->sli_image }}" alt="" style="height: 5rem;">
                    </td>
                    <td>
                        {{ $item->position->pos_name }}
                    </td>
                    <td>
                        @if ($item->sli_status == 1)
                        <span class="badge badge-success t-change-info" title="change" data-id="{{ $item->id }}" data-type='sli_status'>Active</span>
                        @else
                        <span class="badge badge-warning t-change-info" title="change" data-id="{{ $item->id }}" data-type='sli_status'>Block</span>
                        @endif
                        
                    </td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('form-edit-slider', $item->id) }}" class="dropdown-item"><i class="icon-database-edit2"></i> Sửa</a>
                                    <a href="{{ route('delete-slider', $item->id) }}" class="dropdown-item" onclick="return confirm('Bạn chắc chắn muốn xoá?')"><i class="icon-database-remove" ></i> Xoá</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
             
        </tbody>
    </table>
</div>

@endsection
@push('scripts')
    <script>
         $(document).on('click', '.t-change-info', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                type : type
            }
            let url_ = @json(route('change-info-slider'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $(__this).removeClass('badge-success');
                    $(__this).addClass('badge-warning');
                    $(__this).html('Block');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else if(msg.type == 0){
                    $(__this).removeClass('badge-warning');
                    $(__this).addClass('badge-success');
                    $(__this).html('Active');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu, lỗi : " + jqXHR.responseJSON.message);
            });
            
        })
    </script>
@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/back/global_assets/js/demo_pages/datatables_basic.js"></script>
@endpush

