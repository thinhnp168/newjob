@extends('admin.layout.index')
@section('title_site', 'Thêm mới slider')
@section('content')
<script src='/client/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.t-mce",
    height: 200,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
    </script>
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Thêm mới slider</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="" method="post">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Tên:</label>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="sli_title" placeholder="Nhập tên slider" >
                    @error('sli_title')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Link:</label>
                <div class="col-lg-6">
                    <input type="text" class="form-control" name="sli_link" placeholder="Nhập đường dẫn" >
                    @error('sli_link')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Vị trí:</label>
                <div class="col-lg-6">
                    <select name="position_id" id="" class="form-control " >
                        @foreach ($position as $item)
                            <option value="{{ $item->id }}">{{ $item->pos_name }}</option>
                        @endforeach
                        
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Trạng thái:</label>
                <div class="col-lg-6">
                    <select name="sli_status" id="" class="form-control " >
                        <option value="1">Active</option>
                        <option value="0">Block</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2">Ảnh slider :</label>
                <div class="col-lg-6">
                    <div class="input-group">
                        <span class="input-group-btn">
                        <a id="t-lfm-sli_image" data-input="sli_image" data-preview="sli_image-holder" class="btn btn-warning">
                            <i class="fa fa-picture-o"></i> Chọn ảnh slider
                        </a>
                        </span>
                        <input id="sli_image" class="form-control" type="text" name="sli_image" value="{{ old('sli_image') }}">
                    </div>
                    @error('sli_image')
                        <span class="form-text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div id="sli_image-holder"  class="d-flex justify-content-center mt-3 mb-3"></div>
            <div class="form-group">
                <label class="col-form-label" for="sli_content">Content</label>
                <textarea name="sli_content" class="form-control teacher t-mce" id="sli_content" ></textarea>
                @error('sli_content')
                    <span class="form-text text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group row mb-0">
                <div class="col-lg-10 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@push('scripts')
    
@endpush
@push('theme-js')
<script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('#t-lfm-sli_image').filemanager('image');
    </script>
@endpush

