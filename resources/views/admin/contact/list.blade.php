@extends('admin.layout.index')
@section('title_site', 'Danh sách Liên hệ')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Danh sách Liên hệ</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <table class="table datatable-basic table-bordered t-datatable-basic">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th class="d-none">Name</th>
                <th>Email</th>
                <th>Title</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listContacts as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->cct_name }}</td>
                    <td class="d-none">{{ $item->cct_name }}</td>
                    <td>{{ $item->cct_email }}</td>
                    <td>{{ $item->cct_title }}</td>
                    <td>
                        @if ($item->cct_status == 1)
                            <span class="badge badge-success t-change-info" title="change" data-id="{{ $item->id }}" data-type='cct_status'>Active</span>
                        @else
                            <span class="badge badge-warning t-change-info" title="change" data-id="{{ $item->id }}" data-type='cct_status'>Block</span>
                        @endif
                        
                    </td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="" class="dropdown-item" data-toggle="modal" data-target="#modal_default{{ $item->id }}"><i class="icon-eye"></i> Xem</a>
                                    <a href="" class="dropdown-item" onclick="return confirm('Bạn chắc chắn muốn xoá?')"><i class="icon-database-remove" ></i> Xoá</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <div id="modal_default{{ $item->id }}" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{ $item->cct_name }} - {{ $item->cct_email }}</h5>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <h6 class="font-weight-semibold">{{ $item->cct_title }}</h6>
                                    <hr>
                                    <p>
                                        {!! $item->cct_content !!}
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@push('scripts')
    <script>
        $(document).on('click', '.t-change-info', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                type : type
            }
            let url_ = @json(route('change-info-contact'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                    $(__this).removeClass('badge-success');
                    $(__this).addClass('badge-warning');
                    $(__this).html('Block');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else if(msg.type == 0){
                    $(__this).removeClass('badge-warning');
                    $(__this).addClass('badge-success');
                    $(__this).html('Active');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu, lỗi : " + jqXHR.responseJSON.message);
            });
            
        })
    </script>
@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/back/global_assets/js/demo_pages/components_modals.js"></script>
    <script src="/back/global_assets/js/demo_pages/datatables_basic.js"></script>
@endpush

