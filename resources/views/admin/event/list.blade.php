@extends('admin.layout.index')
@section('title_site', 'Danh sách sự kiện')
@section('content')
<div class="card">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Danh sách sự kiện {{ !empty($user)? 'tạo bởi: ' .$user->urs_name : "" }}</h5> 
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <table class="table datatable-basic table-bordered t-datatable-basic">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tên</th>
                <th>Người tạo</th>
                <th>Status</th>
                <th>Sự kiện hot</th>
                <th>Quan tâm</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listEvents as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->eve_name }}</td>
                    <td>{{ $item->user->urs_name }}</td>
                    <td>
                        <select name="" id="" class="form-control t-change-value" data-id="{{ $item->id }}" data-type='eve_status'>
                            <option value="0" {{ $item->eve_status == 0 ? 'selected' : '' }}>Bản nháp</option>
                            <option value="1" {{ $item->eve_status == 1 ? 'selected' : '' }}>Đã duyệt</option>
                            <option value="2" {{ $item->eve_status == 2 ? 'selected' : '' }}>Không duyệt</option>
                        </select>
                    </td>
                    <td>
                        @if ($item->eve_is_hot == 1)
                            <span class="badge badge-danger t-change-info" title="change" data-id="{{ $item->id }}" data-type='eve_is_hot'>Sự kiện hot</span>
                        @else
                            <span class="badge badge-secondary t-change-info" title="change" data-id="{{ $item->id }}" data-type='eve_is_hot'>Sự kiện thường</span>
                        @endif
                        
                    </td>
                    <td>{{ $item->eve_love }}</td>
                    <td class="text-center">
                        <div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="{{ route('form-edit-event', $item->id) }}" class="dropdown-item"><i class="icon-database-edit2"></i> Sửa</a>
                                    <a href="{{ route('delete-event', $item->id) }}" class="dropdown-item" onclick="return confirm('Bạn chắc chắn muốn xoá?')"><i class="icon-database-remove" ></i> Xoá</a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
@push('scripts')
    <script>
        $(document).on('change', '.t-change-value', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            let value = $(__this).val();
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                value : value,
                type : type
            }
            let url_ = @json(route('change-value-event'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 1) {
                toastr.success(msg.mess);
                $(__this).prop('disabled', false);
                }else {
                toastr.warning(msg.mess);
                $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        })
        $(document).on('click', '.t-change-info', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            $(__this).prop('disabled', true);
            let id = $(__this).attr('data-id');
            let type = $(__this).attr('data-type');
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                id : id,
                type : type
            }
            let url_ = @json(route('change-info-event'));
            let url = url_ + '/' + id;
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
                if (msg.type == 0) {
                    $(__this).removeClass('badge-secondary');
                    $(__this).addClass('badge-danger');
                    $(__this).html('Sự kiện hot');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else if(msg.type == 1){
                    $(__this).removeClass('badge-danger');
                    $(__this).addClass('badge-secondary');
                    $(__this).html('Sự kiện thường');
                    toastr.success(msg.mess);
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
                alert("Không thể gửi yêu cầu mã lỗi : " + textStatus);
            });
            
        })
        
    </script>
@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/back/global_assets/js/demo_pages/datatables_basic.js"></script>
@endpush

