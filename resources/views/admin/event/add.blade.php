@extends('admin.layout.index')
@section('title_site', 'Thêm mới sự kiện')
@section('content')
<script src='/client/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.t-mce",
    height: 200,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
    </script>
<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Thêm mới sự kiện</h6>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
                <a class="list-icons-item" data-action="reload"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        
        <form action="" method="post">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Tên sự kiện:</label>
                        <div class="">
                            <input type="text" class="form-control" name="eve_name" placeholder="Nhập tên" >
                            @error('eve_name')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Xuất bản:</label>
                        <div class="">
                            <select name="eve_status" id="" class="form-control" >
                                <option value="1">Đã duyệt</option>
                                <option value="2">Không duyệt</option>
                                <option value="0">Bản nháp</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Mô tả ngắn:</label>
                        <div class="">
                            <textarea name="eve_description" class="form-control" rows="3"></textarea>
                            @error('eve_description')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="col-form-label ">Ngày bắt đầu :</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-calendar"></i>
                                </span>
                            </span>
                            <input type="text" name="eve_start_date" class="form-control eve_start_date" placeholder="Chọn ngày" autocomplete="off" >
                            @error('eve_start_date')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Ngày kết thúc :</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-calendar"></i>
                                </span>
                            </span>
                            <input type="text" name="eve_end_date" class="form-control eve_end_date" placeholder="Chọn ngày" autocomplete="off" >
                            @error('eve_end_date')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Giờ bắt đầu :</label>
                        <div class="input-group">
                            <input id="" name="eve_start_hour" class="form-control" type="time">
                            @error('eve_end_date')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Giờ kết thúc :</label>
                        <div class="input-group">
                            <input id="" name="eve_end_hour" class="form-control"  type="time">
                            @error('eve_end_date')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="input-group">
                        <label class="col-form-label ">Ảnh bìa :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-eve_image" data-input="eve_image" data-preview="eve_image-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh bìa
                            </a>
                            </span>
                            <input id="eve_image" class="form-control" type="text" name="eve_image" value="{{ old('eve_image') }}">
                        </div>
                        @error('eve_image')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="eve_image-holder"  class="d-flex justify-content-center mt-3 mb-3"></div>
                    <div class="input-group">
                        <label class="col-form-label ">Ảnh đại diện :</label><span class="text-danger font-weight-bold"> *</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                            <a id="t-lfm-eve_avatar" data-input="eve_avatar" data-preview="eve_avatar-holder" class="btn btn-warning">
                                <i class="fa fa-picture-o"></i> Chọn ảnh đại diện
                            </a>
                            </span>
                            <input id="eve_avatar" class="form-control" type="text" name="eve_avatar" value="{{ old('eve_avatar') }}">
                        </div>
                        @error('eve_avatar')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div id="eve_avatar-holder"  class="d-flex justify-content-center mt-3 mb-3"></div>
                </div>
                <div class="col-lg-6 col-12">
                    <div class="form-group ">
                        <label class="col-form-label ">Title:</label>
                        <div class="">
                            <input type="text" class="form-control" name="eve_seo_title" placeholder="Nhập title" >
                            @error('eve_seo_title')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Keyword:</label>
                        <div class="">
                            <input type="text" class="form-control" name="eve_seo_keyword" placeholder="Nhập keyword" >
                            @error('eve_seo_keyword')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Description:</label>
                        <div class="">
                            <textarea name="eve_seo_description" id="" class="form-control" rows="3"></textarea>
                            @error('eve_seo_description')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Sự kiện hot:</label>
                        <div class="">
                            <select name="eve_is_hot" id="" class="form-control" >
                                <option value="0">Sự kiện thường</option>
                                <option value="1">Sự kiện hot</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Loại địa điểm:</label>
                        <div class="">
                            <select name="eve_address_type" id="" class="form-control eve_address_type" >
                                <option value="0">Offline</option>
                                <option value="1">Online</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Địa điểm:</label>
                        <div class="">
                            <input type="text" class="form-control eve_address" name="eve_address" placeholder="Nhập địa điểm" >
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-form-label ">Giá tiền:</label>
                        <div class="">
                            <input type="number" class="form-control" name="eve_price" min="0" value="0">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-form-label" for="teacher">Giới thiệu sự kiện</label>
                <textarea name="eve_introduce" class="form-control teacher t-mce" id="eve_introduce" ></textarea>
                @error('eve_introduce')
                    <span class="form-text text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <label class="col-form-label" for="teacher">Nội dung sự kiện</label>
                <textarea name="eve_content" class="form-control teacher t-mce" id="eve_content" ></textarea>
                @error('eve_content')
                    <span class="form-text text-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <label class="col-form-label" for="teacher">Thông tin giảng viên</label>
                <textarea name="eve_lecturer" class="form-control teacher t-mce" id="eve_lecturer" ></textarea>
                @error('eve_lecturer')
                    <span class="form-text text-danger">{{ $message }}</span>
                @enderror
            </div>

           

            <div class="form-group row mb-0">
                <div class="col-12 ml-lg-auto">
                    <button type="reset" class="btn btn-light">Cancel</button>
                    <button type="submit" class="btn bg-blue ml-3">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@if(session()->has('success_mess'))
		<script>
			let messages = '{{ session()->get('success_mess') }}';
			toastr.success(messages);
		</script>
	@endif
	@if(session()->has('error_mess'))
		<script>
			let mess = '{{ session()->get('error_mess') }}';
			toastr.error(mess);
		</script>
	@endif

@endsection
@push('scripts')

<script>
    $(document).on('change', '.eve_address_type', function (e) {
        e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            if($(__this).val() == 1) {
                $('.eve_address').parent('.form-group').addClass('d-none');
            }else {
                $('.eve_address').parent('.form-group').removeClass('d-none');
            }
     });
    $( document ).ready(function() {
            $('.eve_start_date').datepicker({
                defaultDate: '+1w',
                numberOfMonths: 2,
                dateFormat: 'yy-mm-dd',
                onClose: function(selectedDate) {
                    $('.eve_end_date').datepicker('option', 'minDate', selectedDate);
                },
                isRTL: $('html').attr('dir') == 'rtl' ? true : false
            });

            // To
            $('.eve_end_date').datepicker({
                defaultDate: '+1w',
                numberOfMonths: 2,
                dateFormat: 'yy-mm-dd',
                onClose: function(selectedDate) {
                    $('.eve_start_date').datepicker('option', 'maxDate', selectedDate);
                },
                isRTL: $('html').attr('dir') == 'rtl' ? true : false
            });
        });
</script>
@endpush
@push('theme-js')
    <script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/mousewheel.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/globalize.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.de-DE.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.vi-VN.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js"></script>
    <script src="/back/global_assets/js/demo_pages/jqueryui_forms.js"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $('#t-lfm-eve_image').filemanager('image');
        $('#t-lfm-eve_avatar').filemanager('image');
    </script>
@endpush

