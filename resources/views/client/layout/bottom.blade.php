<div class="container-fluid t-bottom">
    <div class="row">
        <div class="container pt-5 pb-4">
            <div class="row">
                <div class="col-md-9 col-12">
                    <img  loading="lazy" src="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_logo_bot : '/client/image/logo2.png' }}" alt="" class="logo-bot">
                    <div class="row">
                        <div class="col-md-8 col-12">
                            
                            <h3 class="text-uppercase t-tag-line position-relative">{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_tagline : 'MẠNG LƯỚI CHIA SẺ SỰ KIỆN MIỄN PHÍ' }} <span class="t-line-bottom"></span></h3>
                            <p class="mt-5"><span class="icon icon-t-home"></span>Trụ sở chính: {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_adress : 'Số 62, Nguyễn Huy Tưởng' }}</p>
                            <p><span class="icon icon-t-phone"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_phone_hn : '0889922600'}} - {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_phone_hcm : '0889922600' }}</p>
                            <p><span class="icon icon-www"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_website : 'sukienso.com.vn' }}</p>
                        </div>
                        <div class="col-md-4 col-12">
                            <h3 class="text-uppercase t-tag-line position-relative">Danh mục <span class="t-line-bottom"></span></h3>
                            <p class="mt-5"><a href="">Giới thiệu</a></p>
                            <p><a href="">Điều khoản dịch vụ</a></p>
                            <p><a href="">Bảng giá</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-12 d-flex flex-column justify-content-center">
                    <img  loading="lazy" src="/client/image/hop-tac-ban-ve_03.png" alt="">
                    <p class="mt-4 text-center">Kết nối với chúng tôi</p>
                    <p class="text-center">
                        <a href="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_facebook : 'https://www.facebook.com/sukiensovietnam' }}"> <span class="icon icon-t-facebook"></span></a>
                        <a href="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_youtube : 'https://www.youtube.com/channel/UCKqHu4WjlgwPiQfuFIjjcJA'}}"><span class="icon icon-t-youtube"></span></a>
                        <a href="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_zalo : 'https://zalo.me/0889922600' }}"><span class="icon icon-t-zalo"></span></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>