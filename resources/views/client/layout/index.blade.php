<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        @hasSection('title')
            @yield('title')
        @else
            {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_seo_title : 'sukienso.com.vn' }}
        @endif
    </title>
    <meta name="description" content="
                                        @hasSection('description')
                                            @yield('description')
                                        @else
                                            {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_seo_description : '' }}
                                        @endif
                                    " />
    <meta name="keywords" content="
                                    @hasSection('keywords')
                                        @yield('keywords')
                                    @else
                                        {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_seo_keyword : ''}}
                                    @endif
                                    
                                " />
    <link rel="icon" type="image/png" sizes="16x16"  href="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_seo_favicon : '/client/image/favicon-16x16.png' }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <meta name="__token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/client/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/client/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-KA6wR/X5RY4zFAHpv/CnoG2UW1uogYfdnP67Uv7eULvTveboZJg0qUpmJZb5VqzN" crossorigin="anonymous">
    
    <link href="/back/assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
	<link href="/back/assets/font-awesome/css/font-awesome.min.css" type="text/css" >
	<link href="/back/assets/css/toastr.min.css" rel="stylesheet" type="text/css">
    @stack('theme-css')
    <link rel="stylesheet" href="/client/css/t.css">
    <link rel="stylesheet" media="screen and (max-width: 576px)" href="/client/css/responsive-mobile.css" />
    <link rel="stylesheet" media="screen and (min-width: 576px) and (max-width: 960px)" href="/client/css/responsive-tablet.css" />
</head>
<body>
    
    @include('client.layout.top')
    @yield('content')
    @include('client.layout.bottom')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    
    <script src="/client/js/owl.carousel.min.js"></script>
    <script src="/back/assets/js/toastr.min.js"></script>
    <script src="/client/js/t.js"></script>
   
    @stack('theme-js')
    @stack('scripts')
    @if(session()->has('success_mess'))
    <script>
            let messages = '{{ session()->get('success_mess') }}';
            toastr.success(messages);
        </script>
    @endif
    @if(session()->has('error_mess'))
        <script>
            let mess = '{{ session()->get('error_mess') }}';
            toastr.error(mess);
        </script>
    @endif
</body>
</html>