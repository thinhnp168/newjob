<div class="container-fluid t-top">
    <div class="row">
        <div class="container">
            <nav class="navbar navbar-expand-sm navbar-light">
                <a class="navbar-brand" href="/">
                    <img src="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_logo_top : '/client/image/logo-sukienso.png' }}" alt="" class="logo">
                    <img src="{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_logo_top_small : '/client/image/logo-sukienso-small.png' }}" alt="" class="logo-tablet">
                </a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavId">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 t-sub-menu-top1">
                        <li class="nav-item active">
                            <a class="nav-link text-uppercase" href="/"><span class="t-text-menu"> Trang chủ</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{ route('about') }}"><span class="t-text-menu"> Giới thiệu</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{ route('client-list-event') }}"><span class="t-text-menu"> Sự kiện</span></a>
                            <ul class="t-sub-menu-top2 ">
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" href="{{ route('dashboard-event-detail', ['year-end-party-la-gi', 3]) }}"><span class="t-text-menu"> Sự kiện 1</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" href=""><span class="t-text-menu"> Giới thiệu</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" href=""><span class="t-text-menu"> Giới thiệu</span></a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{ route('category') }}"><span class="t-text-menu"> Tin tức</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="{{ route('client-form-contact') }}"><span class="t-text-menu"> Liên hệ</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-uppercase" href="#" type="button" data-toggle="modal" data-target="#t-home-searchModal"><span class="icon icon-search"></span></a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 t-sub-menu-top1">
                        @if (Auth::check())
                            <li class="nav-item ">
                                <a class="nav-link text-uppercase btn-taosukien" href="{{ route('dashboard-form-add-event') }}"><span class="icon icon-plus"></span><span class="t-text-menu">Tạo sự kiện</span></a>
                            </li>
                            <li class="nav-item dropdown d-flex align-items-center">
                                <a class="nav-link text-uppercase dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="t-text-menu">{!! Auth::user()->urs_name !!}</span></a>
                                <div class="dropdown-menu" aria-labelledby="dropdownId">
                                    <a class="dropdown-item " href="{{ route('dashboard', Auth::user()->id) }}"><i class="fa fa-list-alt"></i><span class="t-text-menu"> Quản lý sự kiện</span></a>
                                    <a class="dropdown-item " href="{{ route('logOut') }}"><i class="fas fa-power-off"></i><span class="t-text-menu"> Đăng xuất</span></a>
                                </div>
                            </li>
                        @else
                        <li class="nav-item d-flex align-middle flex-column justify-content-center">
                            <a  class="nav-link " href="{{ route('showLogin') }}"><span class="icon icon-user"></span><span class="btn-dangnhap">Đăng nhập</span></a>
                        </li>
                        <li class="nav-item d-flex align-middle flex-column justify-content-center">
                            <a  class="nav-link " href="{{ route('showRegister') }}"><span class="icon icon-user"></span><span class="btn-dangnhap">Đăng ký</span></a>
                        </li>
                        @endif
                      
                    </ul>   
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="modal fade" id="t-home-searchModal" tabindex="-1" role="dialog" aria-labelledby="t-home-searchModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="t-home-searchModalLabel">Tìm kiếm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <form class="form-inline">
                <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="form-control" id="" placeholder="Nhập từ khoá">
                </div>
                <button type="submit" class="btn btn-primary btn-home-search mb-2">Tìm kiếm</button>
            </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-scroll-to-top" id="btn-scroll-to-top"><i class="far fa-arrow-alt-circle-up"></i></button>
@push('scripts')
    <script>
        mybutton = document.getElementById("btn-scroll-to-top");
 
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if($('.t-card-khoahoc-to-top').length > 0) {
                if ((document.body.scrollTop > 950 & document.body.scrollTop < 1950)  || (document.documentElement.scrollTop > 950 & document.documentElement.scrollTop < 1950)) {
                    $('.t-card-khoahoc-to-top').addClass('t-fix-top');
                } else {
                    $('.t-card-khoahoc-to-top').removeClass('t-fix-top');
                }
                
            }
            if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
            
        }

        mybutton.addEventListener("click", smoothScrollBackToTop);

        function smoothScrollBackToTop() {
            const targetPosition = 0;
            const startPosition = window.pageYOffset;
            const distance = targetPosition - startPosition;
            const duration = 750;
            let start = null;
            
            window.requestAnimationFrame(step);

            function step(timestamp) {
                if (!start) start = timestamp;
                const progress = timestamp - start;
                window.scrollTo(0, easeInOutCubic(progress, startPosition, distance, duration));
                if (progress < duration) window.requestAnimationFrame(step);
            }
        }

        function easeInOutCubic(t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t*t + b;
            t -= 2;
            return c/2*(t*t*t + 2) + b;
        };

    </script>
@endpush