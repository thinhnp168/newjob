<div class="container-fluid cover-all-slide">
    <div class="row position-relative">
        <div class="owl-carousel owl-theme main-slide">
            @foreach ($sliderTop as $item)
            <div class="item">
                <img src="{{ $item->sli_image }}" class="d-block w-100" alt="{{ $item->title }}">
            </div>
            @endforeach
        </div>
        <div class="container cover-small-slide">
            <div class="owl-carousel owl-theme small-slide">
                @foreach ($sliderSemiTop as $item)
                <div class="item">
                    <img src="{{ $item->sli_image }}" class="d-block w-100" alt="{{ $item->title }}">
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>