@extends('client.layout.index')
@section('title', 'Giới thiệu')
@section('content')
<div class="container-fluid t-gioithieu1 pb-4">
    <div class="row">
        <div class="container pt-4">
            <div class="row">
                <div class="col-md-7 col-12">
                    <h1 class="main-title">Giới thiệu về sự kiện số</h1>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Sự kiện số - Kênh chia sẻ thông tin lớn nhất Việt Nam truyền tải đầy đủ tất cả các sự kiện đa ngành mang tính hot nhất, được quan tâm nhất.</p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Sân chơi không giới hạn dành cho những cá nhân, tổ chức, doanh nghiệp muốn giới thiệu, quảng bá các sự kiện ý nghĩa đến với cộng động. </p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Giải pháp truyền thông 5.0 hiệu quả, chuyên nghiệp, tiết kiệm thời gian, chi phí, nhân sự.  </p>
                </div>
                <div class="col-md-5 col-12">
                    <figure>
                        <img  loading="lazy" src="/client/image/gioithieu1.png" alt="">
                    </figure>
                    
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-5 col-12">
                    <figure>
                        <img  loading="lazy" src="/client/image/gioithieu2.png" alt="">
                    </figure>
                </div>
                <div class="col-md-7 col-12">
                    <h2 class="main-title">Điểm khác biệt của chúng tôi</h2>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Mạng lưới chia sẻ tạo sự kiện miễn phí</p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Người dùng đăng ký tham gia nhanh chóng, dễ dàng </p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Sự kiện đa ngành, lĩnh vực chuyên sâu, phong phú: Hội nghị, Triển lãm, Công nghệ, Hội thảo, Giải trí, ....</p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Đối tác chuyên nghiệp</p>
                    <p class="position-relative"><span class="icon icon-list-style"></span>Tính Viral cao </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid t-gioithieu2">
    <div class="row">
        <div class="container">
            <h2 class="title pt-4 pb-4">Khách hàng nói về sự kiện số</h2>
            <div class="owl-carousel owl-theme t-report-slide pb-5">
                @foreach ($sliderFeedBack as $item)
                <div class="item">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-7 col-12 text-center">
                            <img  loading="lazy" class="t-customer-avatar " src="{{ $item->sli_image }}" alt="{{ $item->sli_title }}">
                            <div class="text-center position-relative pl-5 mt-3"><span class="icon icon-customer"></span>
                                {!! $item->sli_content !!}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container-fluid t-gioithieu3">
    <div class="row">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-12">
                    <p class="position-relative title"><span class="icon icon-hand-phone"></span>Đăng ký số điện thoại
                        để nhận tư vấn</p>
                </div>
                <div class="col-md-6 col-12 d-flex">
                    <form action="{{ route('client-create-phone') }}" method="post" class="form-inline">
                        @csrf
                        <div class="form-group mb-2 mr-3">
                            <input type="text" class="t-about-number-phone form-control" name="pho_phone" placeholder="Số điện thoại" autocomplete="off">
                        </div>
                        <button type="submit" class="btn btn-nhantuvan mb-2">Nhận tư vấn</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
@error('pho_phone')
<script>
    let mess = '{{ $message }}';
    toastr.error(mess);
</script>
@enderror
@endpush