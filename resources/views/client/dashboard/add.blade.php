@extends('client.layout.index')
@section('title', 'Thêm mới sự kiện ')
@section('content')
<link rel="stylesheet" href="/client/css/t-dashboard.css">
<script src='/client/tinymce4/tinymce.min.js'></script>

<script type="text/javascript">
    var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
    </script>

<div class="container-fluid t-dashboard bgr-efefef">
    
    <div class="row ">
        <div class="col-12 col-lg-3 p-0">
            @include('client.dashboard.nav')
        </div>
        <div class="col-12 col-lg-9">
            <form action="" method="post">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <h1 class="main-title">Thêm mới sự kiện</h1>
                    </div>
                    <div class="card-body">
                        <div class="card">
                            <div class="card-header">
                                Thông tin
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="eve_name">Tên sự kiện</label><span class="text-danger font-weight-bold"> *</span>
                                    <input type="text" class="form-control" id="eve_name" name="eve_name" placeholder="Nhập tên" value="{{ old('eve_name') }}">
                                    @error('eve_name')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Mô tả ngắn</label><span class="text-danger font-weight-bold"> *</span>
                                    <textarea name="eve_description" class="form-control" >{!! old('eve_description') !!}</textarea>
                                    @error('eve_description')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Ngày bắt đầu</label><span class="text-danger font-weight-bold"> *</span>
                                    <input type="text" name="eve_start_date" class="form-control eve_start_date" placeholder="Chọn ngày" autocomplete="off" value="{{ old('eve_start_date') }}">
                                    @error('eve_start_date')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Ngày kết thúc</label><span class="text-danger font-weight-bold"> *</span>
                                    <input type="text" name="eve_end_date" class="form-control eve_end_date" placeholder="Chọn ngày" autocomplete="off" value="{{ old('eve_end_date') }}">
                                    @error('eve_end_date')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group ">
                                    <label class="col-form-label ">Giờ bắt đầu :</label>
                                    <div class="input-group">
                                        <input id="" name="eve_start_hour" class="form-control" type="time" value="{{ old('eve_start_hour') }}">
                                        
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label class="col-form-label ">Giờ kết thúc :</label>
                                    <div class="input-group">
                                        <input id="" name="eve_end_hour" class="form-control"  type="time" value="{{ old('eve_end_hour') }}">
                                        
                                    </div>
                                </div>
                                <div class="input-group">
                                    <label class="col-form-label ">Ảnh bìa :</label><span class="text-danger font-weight-bold"> *</span>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <a id="t-lfm-eve_image" data-input="eve_image" data-preview="eve_image-holder" class="btn btn-warning">
                                            <i class="fa fa-picture-o"></i> Chọn ảnh bìa
                                        </a>
                                        </span>
                                        <input id="eve_image" class="form-control" type="text" name="eve_image" value="{{ old('eve_image') }}">
                                    </div>
                                    @error('eve_image')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div id="eve_image-holder"  class="d-flex justify-content-center mt-3 mb-3"></div>
                                <div class="input-group">
                                    <label class="col-form-label ">Ảnh đại diện :</label><span class="text-danger font-weight-bold"> *</span>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                        <a id="t-lfm-eve_avatar" data-input="eve_avatar" data-preview="eve_avatar-holder" class="btn btn-warning">
                                            <i class="fa fa-picture-o"></i> Chọn ảnh đại diện
                                        </a>
                                        </span>
                                        <input id="eve_avatar" class="form-control" type="text" name="eve_avatar" {{ old('eve_avatar') }}>
                                    </div>
                                    @error('eve_avatar')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div id="eve_avatar-holder"  class="d-flex justify-content-center mt-3 mb-3"></div>
                                <div class="form-group">
                                    <label for="">Loại địa điểm</label>
                                    <select name="eve_address_type" id="" class="form-control eve_address_type" >
                                        <option value="0" {{ old('eve_address_type') == 0 ? 'selected' : '' }}>Offline</option>
                                        <option value="1" {{ old('eve_address_type') == 1 ? 'selected' : '' }}>Online</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Địa điểm</label>
                                    <input type="text" class="form-control eve_address"  name="eve_address" placeholder="Nhập địa điểm" value="{{ old('eve_address') }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Giá tiền</label>
                                    <input type="number" class="form-control" name="eve_price" min="0" value="{{ old('eve_address')? old('eve_address') : 0 }}">
                                </div>
                                
                                <div class="form-group">
                                    <label for="eve_introduce">Giới thiệu sự kiện</label><span class="text-danger font-weight-bold"> *</span>
                                    <textarea name="eve_introduce" class="form-control my-editor" id="eve_introduce" >{!! old('eve_introduce') !!}</textarea>
                                    @error('eve_introduce')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="eve_content">Nội dung sự kiện</label><span class="text-danger font-weight-bold"> *</span>
                                    <textarea name="eve_content" class="form-control my-editor" id="eve_content" >{!! old('eve_content') !!}</textarea>
                                    @error('eve_content')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="eve_lecturer">Thông tin giảng viên</label><span class="text-danger font-weight-bold"> *</span>
                                    <textarea name="eve_lecturer" class="form-control my-editor" id="eve_lecturer" >{!! old('eve_lecturer') !!}</textarea>
                                @error('eve_lecturer')
                                    <span class="form-text text-danger">{{ $message }}</span>
                                @enderror
                                </div>
                            </div>
                        </div>
                        <div class="card mt-3 mb-3">
                            <div class="card-header">
                            SEO 
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Title</label><span class="text-danger font-weight-bold"> *</span>
                                    <input type="text" class="form-control" name="eve_seo_title" placeholder="Nhập title" value="{{ old('eve_seo_title') }}">
                                    @error('eve_seo_title')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="description">Key word</label><span class="text-danger font-weight-bold"> *</span>
                                    <input type="text" class="form-control" name="eve_seo_keyword" placeholder="Nhập keyword" value="{{ old('eve_seo_keyword') }}">
                                    @error('eve_seo_keyword')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="keyword">Description</label><span class="text-danger font-weight-bold"> *</span>
                                    <textarea name="eve_seo_description" id="" class="form-control" >{!! old('eve_seo_description') !!}</textarea>
                                    @error('eve_seo_description')
                                        <span class="form-text text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pl-5 pr-5">
                            <i class="fa fa-save"></i> Thêm
                        </button>
                        <button type="reset" class="btn btn-danger pl-5 pr-5">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                    
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@push('scripts')
<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $('#t-lfm-eve_image').filemanager('image');
    $('#t-lfm-eve_avatar').filemanager('image');
</script>
 <script>
     $(document).on('change', '.eve_address_type', function (e) {
        e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-top-right"
            }
            const __this = this;
            if($(__this).val() == 1) {
                $('.eve_address').parent('.form-group').addClass('d-none');
            }else {
                $('.eve_address').parent('.form-group').removeClass('d-none');
            }
     });
     $(document ).ready(function() {
            $('.eve_start_date').datepicker({
                defaultDate: '+1w',
                numberOfMonths: 2,
                dateFormat: 'yy-mm-dd',
                onClose: function(selectedDate) {
                    $('.eve_end_date').datepicker('option', 'minDate', selectedDate);
                },
                isRTL: $('html').attr('dir') == 'rtl' ? true : false
            });

            // To
            $('.eve_end_date').datepicker({
                defaultDate: '+1w',
                numberOfMonths: 2,
                dateFormat: 'yy-mm-dd',
                onClose: function(selectedDate) {
                    $('.eve_start_date').datepicker('option', 'maxDate', selectedDate);
                },
                isRTL: $('html').attr('dir') == 'rtl' ? true : false
            });
        });
 </script>
 @endpush
 @push('theme-css')
 <link href="/back/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
 <link href="/back/assets/css/layout.min.css" rel="stylesheet" type="text/css">
 <link href="/back/assets/css/components.min.css" rel="stylesheet" type="text/css">
 <link href="/back/assets/css/colors.min.css" rel="stylesheet" type="text/css">
 <link href="/back/assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
 <link href="/back/assets/font-awesome/css/font-awesome.min.css" type="text/css" >
 <link href="/back/assets/css/fontawesome-all.min.css" rel="stylesheet" type="text/css">
	<link href="/back/assets/css/toastr.min.css" rel="stylesheet" type="text/css">
 @endpush
 @push('theme-js')
 <script src="/back/global_assets/js/main/jquery.min.js"></script>
	<script src="/back/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="/back/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="/back/assets/js/app.js"></script>
    <script src="/back/global_assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/back/global_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/jquery_ui/widgets.min.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/jquery_ui/effects.min.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/mousewheel.min.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/globalize.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.de-DE.js"></script>
	<script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.vi-VN.js"></script>
    <script src="/back/global_assets/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.ja-JP.js"></script>
	<script src="/back/global_assets/js/demo_pages/jqueryui_forms.js"></script>
    <script src="/back/assets/js/toastr.min.js"></script>

@endpush