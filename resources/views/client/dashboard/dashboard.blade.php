@extends('client.layout.index')
@section('title', 'Sự kiện số')
@section('content')
<link rel="stylesheet" href="/client/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/client/css/t-dashboard.css">
<div class="container-fluid t-dashboard bgr-efefef">
    
    <div class="row ">
        <div class="col-12 col-lg-3 p-0">
            @include('client.dashboard.nav')
        </div>
        <div class="col-12 col-lg-9">

            <div class="card-header mb-2">
                <h1 class="main-title">Danh sách sự kiện của quý khách</h1>
            </div>
            
            <table id="example" class="table table-striped table-bordered " style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Tên sự kiện</th>
                <th>Ảnh đại diện</th>
                <th>Trạng thái</th>
                <th>Quan tâm</th>
                <th>Sửa</th>
                <th>Xoá</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listEvents as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->eve_name }}</td>
                <td>
                    <img src="{{ $item->eve_image }}" alt="" style="height: 5rem;">
                </td>
                <td>
                    <select name="" id="" class="form-control " disabled >
                        <option value="0" {{ $item->eve_status == 0 ? 'selected' : '' }}>Bản nháp</option>
                        <option value="1" {{ $item->eve_status == 1 ? 'selected' : '' }}>Đã duyệt</option>
                        <option value="2" {{ $item->eve_status == 2 ? 'selected' : '' }}>Không duyệt</option>
                    </select>
                </td>
                
                <td>{{ $item->eve_love }}</td>
                <td >
                    <a href="{{ route('dashboard-form-edit-event', [$item->eve_slug, $item->id]) }}" class="btn btn-sm btn-warning">Sửa</a>
                </td>
                <td >
                    <a href="{{ route('dashboard-delete-event', [$item->eve_slug, $item->id]) }}" onclick="confirm('Bạn chắc chắn muốn xoá?')" class="btn btn-sm btn-danger">Xoá</a>
                </td>
            </tr>
            @endforeach
            
           
        </tbody>
    </table>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="/client/js/dataTables.bootstrap4.min.js"></script>
<script src="/client/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endpush