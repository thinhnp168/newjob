<nav class="navbar navbar-expand-lg t-navbar-dasbroard navbar-light bgr-e1e1e1">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @if (Auth::check())
        <ul class="navbar-nav mr-auto d-flex flex-column w-100 list-group">
            <li class="nav-item list-group-item active">
                <a class="nav-link" >Quản lý sự kiện<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item list-group-item">
                
                <a class="nav-link" href="{{ route('dashboard', Auth::user()->id) }}"><i class="fa fa-list mr-2"></i> Danh sách sự kiện</a>
            </li>
            <li class="nav-item list-group-item">
                
                <a class="nav-link" href="{{ route('dashboard-form-add-event') }}"><i class="fa fa-plus mr-2"></i> Thêm mới sự kiện</a>
            </li>
        </ul>
        @endif
        
    </div>
</nav>