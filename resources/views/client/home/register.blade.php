<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Đăng ký
    </title>
    <link rel="icon" type="image/png" sizes="16x16"  href="/client/image/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/client/css/t.css">
</head>
<body>
    <div class="container-fluid bgr-register t-register">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="col-md-6 col-lg-4 col-sm-9 col-12 t-form-dangky">
                <div class="card">
                    <div class="card-header d-flex justify-content-center">
                    <a href="/" title="Trang chủ"><img src="/client/image/logo2.png" alt=""></a>
                    </div>
                    <div class="card-body">
                    <h5 class="card-title text-center text-uppercase">Đăng ký thành viên</h5>
                    <form action="" method="post" class="t-form-register">
                        <div class="col-auto">
                            <div class="form-row mb-2 position-relative">
                                <span class="icon icon-name"></span>
                                <input type="text" class="form-control t-input-register mb-2" name="name" placeholder="Họ và tên">
                            </div>
                            <div class="form-row mb-2 position-relative">
                                <span class="icon icon-mail"></span>
                                <input type="text" class="form-control t-input-register mb-2" placeholder="Email nhận vé">
                            </div>
                            <div class="form-row mb-2 position-relative">
                                <span class="icon icon-register-phone"></span>
                                <input type="text" class="form-control t-input-register mb-2" placeholder="Số điện thoại">
                            </div>
                            <div class="form-row mb-2 position-relative">
                                <span class="icon icon-pass"></span>
                                <input type="password" class="form-control t-input-register mb-2" placeholder="Mật khẩu">
                            </div>
                            <div class="form-row mb-2 position-relative">
                                <span class="icon icon-repass"></span>
                                <input type="password" class="form-control t-input-register mb-2" placeholder="Nhập lại mật khẩu">
                            </div>
                            <div class="form-row mb-2 d-flex justify-content-center">
                                <button type="submit" class="btn btn-register mb-2">Đăng ký</button>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>