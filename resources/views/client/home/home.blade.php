@extends('client.layout.index')
@section('title', 'Sự kiện số')
@section('content')
@include('client.layout.slide')
@php
    use \Illuminate\Support\Str;
@endphp
<div class="container-fluid bgr-efefef t-home-bao-tabs-sukien">
  <div class="row">
    <div class="container">
      <ul class="nav nav-tabs justify-content-center t-home-nav-tabs-sukien" id="" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="sapdienra-tab" data-toggle="tab" href="#sapdienra" role="tab" aria-controls="sapdienra" aria-selected="false"><span>Sắp diễn ra</span> </a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="dangdienra-tab" data-toggle="tab" href="#dangdienra" role="tab" aria-controls="dangdienra" aria-selected="true"><span>Đang diễn ra</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="moidang-tab" data-toggle="tab" href="#moidang" role="tab" aria-controls="moidang" aria-selected="false"><span>Mới đăng</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link " id="mienphi-tab" data-toggle="tab" href="#mienphi" role="tab" aria-controls="mienphi" aria-selected="true"><span>Miễn phí</span></a>
        </li>
      </ul>
      <div class="tab-content" id="">
        <div class="tab-pane fade  " id="dangdienra" role="tabpanel" aria-labelledby="dangdienra-tab">
          <div class="row">
            @foreach ($eventDangdienra as $item)
              <div class="col-12 col-lg-3 col-sm-6 mb-4">
                <div class="card t-card-khoahoc">
                  <figure>
                  <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                  </figure>
                  <div class="card-body">
                    <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                    <div class="row">
                      <div class="col-6 pr-0">
                        <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                      </div>
                      <div class="col-6 pr-0">
                        <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                      </div>
                    </div>
                    <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                    <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                    <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                    {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                  </div>
                </div>
              </div> 
            @endforeach
          </div>
        </div>
        <div class="tab-pane fade  " id="mienphi" role="tabpanel" aria-labelledby="mienphi-tab">
          <div class="row">
            @foreach ($eventFree as $item)
              <div class="col-12 col-lg-3 col-sm-6 mb-4">
                <div class="card t-card-khoahoc">
                  <figure>
                  <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                  </figure>
                  <div class="card-body">
                    <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                    <div class="row">
                      <div class="col-6 pr-0">
                        <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                      </div>
                      <div class="col-6 pr-0">
                        <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                      </div>
                    </div>
                    <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                    <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                    <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                    {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                  </div>
                </div>
              </div> 
            @endforeach
          </div>
        </div>
        <div class="tab-pane fade" id="moidang" role="tabpanel" aria-labelledby="moidang-tab">
          <div class="row">
            @foreach ($eventNewCreat as $item)
              <div class="col-12 col-lg-3 col-sm-6 mb-4">
                <div class="card t-card-khoahoc">
                  <figure>
                  <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                  </figure>
                  <div class="card-body">
                    <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                    <div class="row">
                      <div class="col-6 pr-0">
                        <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                      </div>
                      <div class="col-6 pr-0">
                        <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                      </div>
                    </div>
                    <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                    <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                    <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                    {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                  </div>
                </div>
              </div> 
            @endforeach
          </div>
        </div>
        <div class="tab-pane fade show active" id="sapdienra" role="tabpanel" aria-labelledby="sapdienra-tab">
          <div class="row">
            @foreach ($eventSapdienra as $item)
              <div class="col-12 col-lg-3 col-sm-6 mb-4">
                <div class="card t-card-khoahoc">
                  <figure>
                  <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                  </figure>
                  <div class="card-body">
                    <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                    <div class="row">
                      <div class="col-6 pr-0">
                        <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                      </div>
                      <div class="col-6 pr-0">
                        <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                      </div>
                    </div>
                    <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                    <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                    <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                    {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                  </div>
                </div>
              </div> 
            @endforeach
              
            
          </div>
        </div>
      </div>
      <p class="text-center mt-2"><a href="{{ route('client-list-event') }}" class="btn t-btn-taithemkhoahoc">Xem thêm</a></p>
    </div>
    <div class="container t-home-instruction mb-3">
      <div class="row bao-content">
        <div class="col-lg-4 col-12 ">
            <p class="title position-relative">
              <span class="icon icon-checked-1"></span>
              <span class="t-title-instruction">Chọn sự kiện và loại vé</span> <br />
              <span class="text-muted">Chỉ trong vài click</span>
              <span class="icon icon-chevron-right"></span>
            </p>
        </div>
        <div class="col-lg-4 col-12 ">
            <p class="title position-relative">
              <span class="icon icon-dollar"></span>
              <span class="t-title-instruction">Mua vé trực tiếp từ Ban Tổ Chức</span> <br />
              <span class="text-muted">Thanh toán khi nhận vẽ hoặc online</span>
              <span class="icon icon-chevron-right"></span>
            </p>
        </div>
        <div class="col-lg-4 col-12 ">
            <p class="title position-relative">
              <span class="icon icon-ticked-1"></span>
              <span class="t-title-instruction">Nhận vé</span> <br />
              <span class="text-muted">Qua mail hoặc nhận vé trực tiếp</span>
            </p>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- Sự kiện số giúp gì cho bạn --}}
<div class="container-fluid t-home-sukiensogiupgi bgr-efefef">
  <h1 class="title">SUKIENSO.COM.VN GIÚP GÌ CHO BẠN?</h1>
  <div class="row">
    
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-3 col-sm-6 col-9">
          <p><span class="icon icon-checked"></span>Tạo sự kiện miễn phí</p>
          <p><span class="icon icon-checked"></span>Đặt lịch gửi Email và SMS</p>
          <p><span class="icon icon-checked"></span>Gọi xác nhận tham dự</p>
        </div>
        <div class="col-lg-3 col-sm-6 col-9">
          <p><span class="icon icon-checked"></span>Truyền thông, quảng cáo</p>
          <p><span class="icon icon-checked"></span>In vé, giao vé, thu tiền</p>
          <p><span class="icon icon-checked"></span>Hỗ trợ checkin tại event</p>
        </div>
      </div>
      <p class="text-center mb-0">
        <a href="" class="btn t-btn-taosukien"><span class="icon icon-taosukien"></span>Tạo sự kiện</a>
      </p>
    </div>
  </div>
</div>
<div class="container-fluid t-home-tintuc bgr-efefef pb-4">
  <div class="row">
    <div class="container">
      <h2 class="title text-center pt-3 pb-3"><span>Tin tức</span></h2>
      <div class="row">
        <div class="col-md-6 col-12">
          <div class="card t-home-card-tintucto">
            <a href="{{ route('post-detail', [$post[0]->pts_slug, $post[0]->id]) }}"><figure><img src="{{ $post[0]->pts_image }}"  loading="lazy" class="card-img-top" alt="{{ $post[0]->pts_name }}"></figure></a>
            <div class="card-body">
              <a href="{{ route('post-detail', [$post[0]->pts_slug, $post[0]->id]) }}">
                <h5 class="card-title">{{ $post[0]->pts_name }}</h5></a>
              <p class="card-text">{!!  Str::limit($post[0]->pts_description, 300, '...') !!}</p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-12">
          @foreach ($post as $key => $item)
            @if ($key != 0)
            <div class="card mb-2 t-home-card-tintucnho" >
              <div class="row no-gutters">
                <div class="col-3">
                  <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><img src="{{ $item->pts_image }}"  loading="lazy" class="card-img" alt="{{ $item->pts_name }}"></a>
                </div>
                <div class="col-9 d-flex align-items-stretch">
                  <div class="card-body">
                    <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"> <h5 class="card-title">{{ $item->pts_name }}</h5></a>
                  </div>
                </div>
              </div>
            </div>
            @endif
          @endforeach
          
          
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid t-home-doitac bgr-efefef">
  <div class="row">
    <div class="container">
      <h2 class="title text-center pt-3 pb-3"><span> Đối tác của chúng tôi </span></h2>
      <div class="owl-carousel owl-theme doitac-slide">
        @foreach ($sliderDoitac as $item)
          <div class="item">
            <img src="{{ $item->sli_image }}" alt="{{ $item->sli_title }}"  loading="lazy">
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

@endsection