@extends('client.layout.index')
@section('title', 'Chi tiết tin tức')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
            <li class="breadcrumb-item " aria-current="page"><a href="{{ route('category') }}">Tin tức</a>  </li>
            <li class="breadcrumb-item active" aria-current="page">{{ $postDetail->pts_name }}</li>
          </ol>
        </nav>
    </div>
  </div>
</div>
<div class="container-fluid bgr-efefef">
  <div class="row">
    <div class="container t-category t-post-detail">
        <div class="row pt-2">
            <div class="col-lg-8 col-12 border-right mb-4">
                <h1 class="main-title">{{ $postDetail->pts_name }}</h1>
                <div class="row d-flex justify-content-between">
                  <div class="col-md-6 col-12 ">
                    <p class="time text-muted">{{ date_format($postDetail->created_at, "Y/m/d H:i") }}</p>
                  </div>
                  <div class="col-md-6 col-12 ">
                    <div class="row align-items-center justify-content-center">
                      <a href="" class="btn btn-primary btn-like mr-2"><span class="icon icon-like"></span>Thích </a>
                      <a href="" class="btn btn-primary btn-like mr-2">Chia sẻ</a>
                      <a href="" class="btn btn-outline-primary btn-like mr-2" mb-2><span class="icon icon-flag"></span>Lưu vào facebook</a>
                      <a href="" class="btn-secondary d-flex justify-content-center align-items-center"><i class="fa fa-envelope d-flex justify-content-center align-items-center"></i></a>
                    </div>
                  </div>
                </div>
                <div class="t-content mt-2">
                  
                  {!! $postDetail->pts_content !!}

                </div>
                <div class="d-flex flex-row justify-content-end">
                  <a href="" class="btn btn-primary btn-like mr-2">Chia sẻ</a>
                  <a href="" class="btn btn-primary btn-like mr-2"><span class="icon icon-like"></span>Thích 0</a>
                </div>
                <p class="t-title-comment">Bình luận</p>
                <p class="text-muted">Chia sẻ bình luận của bạn về bài viết này</p>
                <p class="t-title-sent-comment text-uppercase">Gửi bình luận của bạn</p>
                <div class="card mb-3 t-card-send-comment t-cover-card-send-reply-comment" >
                    <div class="card-body ">
                      <textarea name="cmt_content" class="t-textarea-comment cmt_content" rows="3"></textarea>
                    </div>
                    <div class="card-footer ">
                      <div class="form-row">
                        <div class="col-md-5 col-12">
                          <input type="text" name="cmt_name" class="form-control t-input-send-comment cmt_name" placeholder="Mời bạn nhập tên (Bắt buộc)">
                        </div>
                        <div class="col-md-5 col-12">
                          <input type="text" name="cmt_email" class="form-control t-input-send-comment cmt_email" placeholder="Mời bạn nhập email (Không bắt buộc)">
                        </div>
                        <div class="col-md-2 col-12">
                          <input class="btn btn-secondary form-control t-btn-send-comment"  data-parent_id="0"  type="submit" value="Gửi" >
                        </div>
                      </div>
                    </div>
                </div>
                <span class="line"></span>
                <div class="card mt-2 t-card-list-comment" >
                  <div class="card-header text-uppercase">
                      Các bình luận
                  </div>
                  <ul class="list-group list-group-flush">
                   @foreach ($postDetail->comment as $item)
                   @if ($item->parent_id == 0)
                   <li class="list-group-item t-item-comment p-0 mt-2 mb-2">
                    <div class="card" >
                      <div class="card-header">
                        <p class="mb-0"><span class="item-title" >{{ $item->cmt_name }} :</span>  {!! $item->cmt_content !!}</p>
                      </div>
                      <ul class="list-group list-group-flush">

                        @foreach ($postDetail->comment as $item2)
                        @if ($item2->parent_id == $item->id)
                          <li class="list-group-item">
                            <p class="mb-0 pl-3"><span class="item-title" >{{ $item2->cmt_name }} :</span>  {{ $item2->cmt_content }}</p>
                          </li>
                        @endif
                        @endforeach
                      </ul>
                    </div>
                    <p class="text-right mb-1 mt-1">
                      <a href="" class="btn btn-primary btn-like mr-2 t-btn-show-hide-card-reply-comment">Trả lời</a>
                    </p>
                    <div class="t-card-reply-comment d-none t-cover-card-send-reply-comment">
                      <div class="form-row pl-3 pr-3">
                        <div class="col-12 mb-2">
                          <textarea name="" id="" class="t-textarea-comment cmt_content" rows="1" placeholder="Mời bạn nhập bình luận của mình!"></textarea>
                        </div>
                      </div>
                      <div class="form-row pl-3 pr-3">
                        <div class="col-md-5 col-12">
                          <input type="text" class="form-control t-input-send-comment cmt_name" placeholder="Mời bạn nhập tên (Bắt buộc)">
                        </div>
                        <div class="col-md-5 col-12">
                          <input type="text"  class="form-control t-input-send-comment cmt_email" placeholder="Mời bạn nhập email (Không bắt buộc)">
                        </div>
                        <div class="col-md-2 col-12">
                          <input class="btn btn-secondary form-control t-btn-send-comment"  data-parent_id="{{ $item->id }}"  type="submit" value="Gửi" >
                        </div>
                      </div>
                    </div>
                    
                  </li>
                       
                   @endif
                     
                   @endforeach
                      
                    
                  </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 t-category-extend">
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Sự kiện hot</a>
                    <a href="" class="d-flex align-items-end t-head-btn-extend">Xem thêm >></a>
                </div>
                <a href="" class=""><figure><img  loading="lazy" src="/client/image/khoa-hoc-1.jpg" alt=""></figure></a>
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Sự kiện mới</a>
                    <a href="" class="d-flex align-items-end t-head-btn-extend">Xem thêm >></a>
                </div>
                <a href="" class=""><figure><img  loading="lazy" src="/client/image/khoa-hoc-1.jpg" alt=""></figure></a>
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Bài viết mới nhất</a>
                </div>
                @foreach ($listnewPosts as $item)
                  <div class="card mb-3 card-extend-post" >
                    <div class="row no-gutters">
                      <div class="col-3 d-flex align-items-center">
                        <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><img  loading="lazy" src="{{ $item->pts_image }}" class="card-img" alt="{{ $item->pts_name }}"></a>
                      </div>
                      <div class="col-9">
                        <div class="card-body p-2 ">
                          <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><h5 class="card-title">{{ $item->pts_name }}</h5></a>
                          <p class="card-text"><small class="text-muted">{{ date_format( $item->created_at, "M d, Y") }}</small></p>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
                  <div class="t-bao-title-extend d-flex justify-content-between">
                    <a href="" class="t-head-title-extend">Truy cập nhanh</a>
                </div>
                <div class="card" >
                    <ul class="list-group list-group-flush t-list-event-extend">
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện đang diễn ra</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện miễn phí</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện mới đăng</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện sắp diễn ra</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện sắp diễn ra</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="container-fluid t-bao-tinlienquan-chitiettin bgr-efefef">
  <div class="row">
    <div class="container">
      <span class="line-dotted"></span>
      <p class="title text-uppercase">Tin liên quan <span class="line"></span></p>
      <div class="row">
        <div class="col-lg-8 col-12">
          <div class="row">
            @foreach ($postRelated as $item)
              <div class="col-md-6 col-12">
                <div class="card mb-3 card-tinlienquan-chitiettin" >
                  <div class="row no-gutters">
                    <div class="col-3 d-flex align-items-center">
                      <a href="" class="card-cover-img"><img  loading="lazy" src="{{ $item->pts_image }}" class="card-img" alt="{{ $item->pts_name }}"></a>
                    </div>
                    <div class="col-9">
                      <div class="card-body p-2">
                        <a href="" class="card-title"><h5 >{{ $item->pts_name }}</h5></a>
                        <p class="card-text"><small class="text-muted">{{ date_format( $item->created_at, "M d, Y") }}</small></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
    <script>
      $(document).on('click', '.t-btn-show-hide-card-reply-comment', function (e) {
        e.preventDefault();
        if ($(this).parents('.t-item-comment').children('.t-card-reply-comment').hasClass('d-none')) {
          $(this).parents('.t-item-comment').children('.t-card-reply-comment').removeClass('d-none');
        } else {
          $(this).parents('.t-item-comment').children('.t-card-reply-comment').addClass('d-none');
        }
        
      })
       $(document).on('click','.t-btn-send-comment', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            let cmt_content = $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_content').val();
            let cmt_name = $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_name').val();
            let cmt_email = $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_email').val();
            let post_id = '{{ $id }}';
            let parent_id = $(__this).attr('data-parent_id');
            $(__this).prop('disabled', true);
            if (cmt_content == '' || cmt_content == null) {
                toastr.warning('Nội dung không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (cmt_name == '' || cmt_name == null) {
                toastr.warning('Tên không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                cmt_content : cmt_content,
                cmt_name : cmt_name,
                cmt_email : cmt_email,
                post_id : post_id,
                parent_id : parent_id
            }
            let url = @json(route('dashboard-creat-comment'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
              console.log(msg);
                if (msg.type == 1) {
                    toastr.success(msg.mess);
                    $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_content').val('');
                    $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_name').val('');
                    $(__this).closest('.t-cover-card-send-reply-comment').find('.cmt_email').val('');
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
              if(jqXHR.responseJSON.errors.cmt_content) {
                toastr.warning(jqXHR.responseJSON.errors.cmt_content);
              }
              if(jqXHR.responseJSON.errors.cmt_name) {
                toastr.warning(jqXHR.responseJSON.errors.cmt_name);
              }
              
            });
        })
    </script>
@endpush