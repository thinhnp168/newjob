@extends('client.layout.index')
@section('title', 'Danh sách tin tức')
@section('content')
@php
    use \Illuminate\Support\Str;
@endphp
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Trang chủ</a></li>
          <li class="breadcrumb-item active" aria-current="page">Tin tức</li>
        </ol>
      </nav>
    </div>
  </div>
</div>
<div class="container-fluid bgr-efefef pb-2">
  <div class="row">
    <div class="container t-category">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="t-bao-title">
                    <p class="t-head-title mb-2">Tin tức</p>
                </div>
                @foreach ($listPosts as $item)
                <div class="card mb-3 t-card-category" >
                  <div class="row no-gutters">
                      <div class="col-md-3 t-card-img">
                          <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"> <figure><img  loading="lazy" src="{{ $item->pts_image }}" class="card-img" alt="{{ $item->pts_name }}"></figure></a>
                      </div>
                      <div class="col-md-9">
                          <div class="card-body">
                            <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><h5 class="card-title">{{ $item->pts_name }}</h5></a>
                            <p class="card-text">{{ $item->pts_description  }}</p>
                            <p class="card-text"><a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}" class="detail">Chi tiết ></a></p>
                          </div>
                        </div>
                    </div>
                  </div>
                @endforeach
                
                {{ $listPosts->render() }}
            </div>
            <div class="col-lg-4 col-12 t-category-extend">
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Sự kiện hot</a>
                    <a href="" class="d-flex align-items-end t-head-btn-extend">Xem thêm >></a>
                </div>
                <a href="" class=""><figure><img  loading="lazy" src="/client/image/khoa-hoc-1.jpg" alt=""></figure></a>
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Sự kiện mới</a>
                    <a href="" class="d-flex align-items-end t-head-btn-extend">Xem thêm >></a>
                </div>
                <a href="" class=""><figure><img  loading="lazy" src="/client/image/khoa-hoc-1.jpg" alt=""></figure></a>
                <div class="t-bao-title-extend d-flex justify-content-between mb-3">
                    <a href="" class="t-head-title-extend">Bài viết mới nhất</a>
                </div>
                @foreach ($listnewPosts as $item)
                  <div class="card mb-3 card-extend-post" >
                    <div class="row no-gutters">
                      <div class="col-3 d-flex align-items-center">
                        <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><img  loading="lazy" src="{{ $item->pts_image }}" class="card-img" alt="{{ $item->pts_name }}"></a>
                      </div>
                      <div class="col-9">
                        <div class="card-body p-2 ">
                          <a href="{{ route('post-detail', [$item->pts_slug, $item->id]) }}"><h5 class="card-title">{{ $item->pts_name }}</h5></a>
                          <p class="card-text"><small class="text-muted">{{ date_format( $item->created_at, "M d, Y") }}</small></p>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
               
                
                  <div class="t-bao-title-extend d-flex justify-content-between">
                    <a href="" class="t-head-title-extend">Truy cập nhanh</a>
                </div>
                <div class="card" >
                    <ul class="list-group list-group-flush t-list-event-extend">
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện đang diễn ra</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện miễn phí</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện mới đăng</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện sắp diễn ra</a></li>
                        <li class="list-group-item"><i class="fa fa-angle-double-right t-fa-angle-double-right mr-2"></i><a href="">Sự kiện sắp diễn ra</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection