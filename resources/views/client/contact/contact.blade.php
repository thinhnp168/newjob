@extends('client.layout.index')
@section('title', 'Giới thiệu')
@section('content')
<div class="container-fluid bgr-efefef t-contact">
    <div class="row">
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <h1 class="main-title mb-4">
                        Liên lệ với {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_website  : 'sukienso.com.vn' }}
                    </h1>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <p class="t-text-contact position-relative"><span class="icon icon-home-ct"></span> {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_adress_hn : 'Tầng 3, Tòa nhà 789, số 147 Hoàng Quốc Việt, phường Nghĩa Đô,quận Cầu Giấy, thành phố Hà Nội' }}</p>
                            <p class="t-text-contact position-relative"><span class="icon icon-phone-ct"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_phone_hn : '0889.922.600' }}</p>
                            <p class="t-text-contact position-relative"><span class="icon icon-email-ct"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_email : 'info@sukienso.com.vn' }}</p>
                        </div>
                        <div class="col-md-6 col-12">
                            <p class="t-text-contact position-relative"><span class="icon icon-home-ct"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_adress_hcm : 'VP.HCM: Tầng 5 - Tòa nhà GIC TOWER Số 326 Cách Mạng Tháng 8 - P10, Q3, TP.HCM' }} </p>   
                            <p class="t-text-contact position-relative"><span class="icon icon-phone-ct"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_phone_hcm : '0889.922.600' }}</p>
                            <p class="t-text-contact position-relative"><span class="icon icon-www-ct"></span>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_website  : 'sukienso.com.vn' }}</p>
                        </div>
                    </div>
                    {!! $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_map : '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7447.338107777216!2d105.799803!3d21.045924!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb13f5dd4b956e22e!2zQ2FvIOG7kGMgNzg5IC0gQuG7mSBRdeG7kWMgUGjDsm5n!5e0!3m2!1svi!2sus!4v1649298902873!5m2!1svi!2sus" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>' !!}
                </div>
                <div class="col-lg-6 col-12">
                    <h2 class="main-title mb-4">
                        Gửi tin nhắn về {{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_website  : 'sukienso.com.vn' }}
                    </h2>
                    <p>Nếu bạn có bất kì câu hỏi hay cần sự trợ giúp từ <strong>{{ $CONFIG_VIEW_SHARE ? $CONFIG_VIEW_SHARE->con_website  : 'sukienso.com.vn' }}</strong> đừng ngần ngại gửi câu hỏi để được sự kiện số hỗ trợ</p>
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="name">Họ và tên</label><span class="text-danger font-weight-bold">*</span>
                            <input type="text" class="form-control" id="name" name="cct_name"  >
                            @error('cct_name')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label><span class="text-danger font-weight-bold">*</span>
                            <input type="email" class="form-control" id="email" name="cct_email" placeholder="">
                            @error('cct_email')
                                <span class="form-text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                          <label for="title">Chủ đề</label>
                          <input type="text" class="form-control" id="title" name="cct_title" placeholder="">
                        </div>
                        <div class="form-group">
                          <label for="content">Nội dung</label>
                          <textarea  class="form-control" id="content" name="cct_content" rows="11"></textarea>
                        </div>
                        <button type="submit" class="btn btn-submit-contact">Gửi</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection