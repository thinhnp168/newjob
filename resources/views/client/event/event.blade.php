@extends('client.layout.index')
@section('title', 'Chi tiết sự kiện')
@section('content')
<div class="container-fluid t-event-detail">
  <div class="row">
    <div class="container">
      <figure><img  loading="lazy" class="mb-3" src="{{ $event->eve_image }}" alt="" width="100%"></figure>
        <div class="row">
            <div class="col-md-8 col-12">
                <h1 class="main-title">{{ $event->eve_name }}</h1>
                <p>{{ $event->eve_description }}</p>
                <p><span class="icon icon-calendar"></span> {{ $event->eve_start_date }} - {{ $event->eve_end_date }} ({{ $event->eve_start_hour }}  - {{ $event->eve_end_hour }})</p>
                <p><span class="icon icon-place"></span>{!! $event->eve_address_type? '<span class="icon icon-online"></span>' : $event->eve_address !!}</p>
            </div>
            <div class="col-md-4 col-12">
                <a href="javascriptVoid(0)" class="btn btn-tuvan" data-toggle="modal" data-target="#CartModal">Tư vấn</a>
                <div class="card-group mt-2">
                    <div class="card card-share-event">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-center"><a href="" class="btn btn-chia-se"><span class="icon icon-face"></span>Chia sẻ</a></li>
                        </ul>
                    </div>
                    <div class="card card-share-event">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-center"><a href="" class="btn btn-quan-tam"><span class="icon icon-quan-tam"></span>Quan tâm</a></li>
                        </ul>
                    </div>
                </div>
                <p class="t-so-quantam">{{ $event->eve_love }} người quan tâm</p>
            </div>
        </div>
      </div> 
    </div>
</div>
<div class="container-fluid t-bao-nav-thongtinsukien">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-12">
          <nav class="nav nav-pills flex-column flex-sm-row tab-thongtin-sukien">
            <a class="flex-sm-fill text-sm-center nav-link active" href="#gioithieukhoahoc">Giới thiệu</a>
            <a class="flex-sm-fill text-sm-center nav-link" href="#noidungkhoahoc">Nội dung</a>
            <a class="flex-sm-fill text-sm-center nav-link" href="#giangvienkhoahoc">Thông tin giảng viên</a>
            <a class="flex-sm-fill text-sm-center nav-link" href="#danhgiacuahocvien">Đánh giá</a>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid bgr-efefef">
  <div class="row">
    <div class="container t-chitiet-sukien">
        <div class="row">
            <div class="col-lg-8 col-12">
                <div class="card mt-2">
                    <div class="card-header">
                        <p id="gioithieukhoahoc">Giới thiệu khoá học</p>   
                    </div>
                    <div class="card-body">
                        {!! $event->eve_introduce !!}
                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-header">
                    <p id="noidungkhoahoc">Nội dung khoá học</p>   
                    </div>
                    <div class="card-body">
                   
                      {!! $event->eve_content !!}

                    </div>
                </div>
                <div class="card mt-2">
                    <div class="card-header">
                    <p id="giangvienkhoahoc">Giảng viên của khoá học</p>   
                    </div>
                    <div class="card-body">
                   
                        {!! $event->eve_lecturer !!}

                    </div>
                </div>
                <div class="card mt-2 t-card-rate-event">
                    <div class="card-header ">
                      <p id="danhgiacuahocvien">Đánh giá của học viên</p>   
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-lg-3 col-12 d-flex justify-content-center flex-column">
                          <p class="t-avg-rate text-center mb-0">{{ $avgStar }}</p>
                          <p class="text-center">
                            <span class="input-rating">
                              <i class="fa fa-star" data-value="1" ></i>
                              <i class="fa fa-star" data-value="2" ></i>
                              <i class="fa fa-star" data-value="3" ></i>
                              <i class="fa fa-star" data-value="4" ></i>
                              <i class="fa fa-star" data-value="5" ></i>
                            </span>
                          </p>
                          <p class="text-center">{{ $totalRate }} đánh giá</p>
                        </div>
                        <div class="col">
                          <div class="row mb-2">
                            <div class="col-7">
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $totalRate5star }}%" aria-valuenow="{{ $totalRate5star }}" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="col p-0">
                              <p class="text-center mb-0">
                                <span class="input-rating">
                                  <i class="fa fa-star" data-value="1" ></i>
                                  <i class="fa fa-star" data-value="2" ></i>
                                  <i class="fa fa-star" data-value="3" ></i>
                                  <i class="fa fa-star" data-value="4" ></i>
                                  <i class="fa fa-star" data-value="5" ></i>
                                </span>
                              </p>
                            </div>
                            <div class="col-1 p-0">
                              <p class="prog mb-0">{{ $totalRate5star }}%</p>
                            </div>
                          </div>
                          <div class="row mb-2">
                            <div class="col-7">
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $totalRate4star }}%" aria-valuenow="{{ $totalRate4star }}" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="col p-0">
                              <p class="text-center mb-0">
                                <span class="input-rating">
                                  <i class="fa fa-star" data-value="1" ></i>
                                  <i class="fa fa-star" data-value="2" ></i>
                                  <i class="fa fa-star" data-value="3" ></i>
                                  <i class="fa fa-star" data-value="4" ></i>
                                  <i class="far fa-star" data-value="5" ></i>
                                </span>
                              </p>
                            </div>
                            <div class="col-1 p-0">
                              <p class="prog mb-0">{{ $totalRate4star }}%</p>
                            </div>
                          </div>
                          <div class="row mb-2">
                            <div class="col-7">
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $totalRate3star }}%" aria-valuenow="{{ $totalRate3star }}" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="col p-0">
                              <p class="text-center mb-0">
                                <span class="input-rating">
                                  <i class="fa fa-star" data-value="1" ></i>
                                  <i class="fa fa-star" data-value="2" ></i>
                                  <i class="fa fa-star" data-value="3" ></i>
                                  <i class="far fa-star" data-value="4" ></i>
                                  <i class="far fa-star" data-value="5" ></i>
                                </span>
                              </p>
                            </div>
                            <div class="col-1 p-0">
                              <p class="prog mb-0">{{ $totalRate3star }}%</p>
                            </div>
                          </div>
                          <div class="row mb-2">
                            <div class="col-7">
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $totalRate2star }}%" aria-valuenow="{{ $totalRate2star }}" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="col p-0">
                              <p class="text-center mb-0">
                                <span class="input-rating">
                                  <i class="fa fa-star" data-value="1" ></i>
                                  <i class="fa fa-star" data-value="2" ></i>
                                  <i class="far fa-star" data-value="3" ></i>
                                  <i class="far fa-star" data-value="4" ></i>
                                  <i class="far fa-star" data-value="5" ></i>
                                </span>
                              </p>
                            </div>
                            <div class="col-1 p-0">
                              <p class="prog mb-0">{{ $totalRate2star }}%</p>
                            </div>
                          </div>
                          <div class="row mb-2">
                            <div class="col-7">
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{ $totalRate1star }}%" aria-valuenow="{{ $totalRate1star }}" aria-valuemin="0" aria-valuemax="100"></div>
                              </div>
                            </div>
                            <div class="col p-0">
                              <p class="text-center mb-0">
                                <span class="input-rating">
                                  <i class="fa fa-star"  ></i>
                                  <i class="far fa-star"  ></i>
                                  <i class="far fa-star"  ></i>
                                  <i class="far fa-star"  ></i>
                                  <i class="far fa-star"  ></i>
                                </span>
                              </p>
                            </div>
                            <div class="col-1 p-0">
                              <p class="prog mb-0">{{ $totalRate1star }}%</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                {{-- <p class="text-right mt-2">
                  <a href="" class="btn btn-warning">Đánh giá</a>
                </p> --}}
                <div class="card mt-3">
                  <div class="card-header">
                    <p>Gửi đánh giá của bạn</p>
                  </div>
                  <div class="card-body">
                    <p>1. Đánh giá của bạn:
                        <span class="input-rating">
                        <i class="fa fa-star t-fa-star" data-value="1" ></i>
                        <i class="fa fa-star t-fa-star" data-value="2" ></i>
                        <i class="fa fa-star t-fa-star" data-value="3" ></i>
                        <i class="fa fa-star t-fa-star" data-value="4" ></i>
                        <i class="fa fa-star t-fa-star" data-value="5" ></i>
                    </span>
                    </p>
                    <input type="hidden" name="" class="danhgia " id="rat_star">
                    <p>2. Tiêu đề của nhận xét. <input type="text" name="" id="rat_title" class="form-control fz12" placeholder="Nhập tiêu đề của nhận xét"></p>
                    
                    <p>3. Viết nhận xét của bạn. <textarea name="" id="rat_content" rows="5" class="form-control" placeholder="Nhận xét của ban về sản phẩm này"></textarea></p>
                    
                    
                    <p class="text-right mt-3"><a href="javascript:void(0)" class="btn-guinhanxet " >Gửi nhận xét</a></p>
                    <p class="text-dieukhoan">* Để nhận xét được duyệt, quý khách lưu ý tham khảo <a class="tieuchi" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModalCenter">Tiêu chí duyệt nhận xét</a> của Sukienso.com.vn</p>
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalCenterTitle">Điều khoản</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                            <div class="modal-body">
                              <p><b>Sukienso.com.vn cảm ơn quý khách đã dành thời gian viết nhận xét về sản phẩm. Trước khi gửi nhận xét, Mong bạn dành ít phút tham khảo hướng dẫn bên dưới:</b></p>
                              <ul>
                                  <li>Nhận xét cần được viết bằng Tiếng Việt có dấu.</li>

                                  <li>Nhận xét không mang tính quảng cáo kêu gọi mua sản phẩm giữa các cá nhân với nhau, không kêu gọi mua sản phẩm từ các website khác, hoặc đã được đăng tải trên các website khác, không cố tình bôi xấu sản phẩm vì lý do cá
                                      nhân.
                                  </li>

                                  <li>Rất mong quý khách có thể viết nhận xét nêu rõ ưu khuyết điểm về sản phẩm trong quá trình sử dụng để giúp các khách hàng khác tham khảo cụ thể hơn về ý kiến khách quan.</li>

                              </ul>
                              <p>Trân trọng.</p>
                            </div>
                      </div>
                    </div>
                    
                    </div>
                  </div>
                </div>
                
                <div class="card mt-2">
                    <div class="card-header">
                        <p>Nhận xét của học viên</p>
                    </div>
                    <ul class="list-group list-group-flush">
                      @foreach ($event->rating as $item)
                        <li class="list-group-item ">
                          <p class="title t-card-rate-event">
                            <span class="input-rating ">
                              @for ($i = 0; $i < 5; $i++)
                                  @if ($i < $item->rat_star)
                                    <i class="fa fa-star"></i>
                                  @endif
                              @endfor
                            </span> 
                            <strong class="pl-2">{{ $item->rat_title }} </strong>  
                        </p>
                        <p>{{ $item->rat_content }}</p>
                      </li>
                      @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-12">
                <div class="card t-card-khoahoc mt-2 t-card-khoahoc-to-top" id="t-card-khoahoc-to-top">
                    <div class="card-body">
                      <h5 class="card-title">{{ $event->eve_name }}</h5>
                      <div class="row">
                        <div class="col-6 pr-0">
                          <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $event->eve_start_date }}</p>
                        </div>
                        <div class="col-6 pr-0">
                          <p></span>08:00 - 17:00</p>
                        </div>
                      </div>
                      <p class="card-text"><span class="icon icon-ticket"></span> Miễn phí</p>
                      <p class="card-text"><span class="icon icon-place"></span> <span class="icon icon-online"></span></p>
                      <a href="#" class="btn btn-primary btn-chitietsukien" data-toggle="modal" data-target="#CartModal">Đăng ký khoá học</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
<div class="container-fluid bgr-efefef pt-3">
  <div class="row">
    <div class="container t-sukienlienquan">
        <p class="title"><span class="icon icon-calendar-color"></span>Sự kiện liên quan</p>
        <div class="row">
          @foreach ($eventRelated as $item)
            <div class="col-12 col-lg-3 col-sm-6 mb-4">
              <div class="card t-card-khoahoc">
                <figure><a href=""><img  loading="lazy" src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}"></a></figure>
                <div class="card-body">
                  <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                  <div class="row">
                    <div class="col-6 pr-0">
                      <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date }}</p>
                    </div>
                    <div class="col-6 pr-0">
                      <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                    </div>
                  </div>
                  <p class="card-text"><span class="icon icon-ticket"></span> {{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }}</p>
                  <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!}</span></p>
                  <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                  {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                </div>
              </div>
            </div> 
          @endforeach
             
            </div> 
        </div> 
    </div>
  </div>
</div>
<div class="modal fade" id="CartModal" tabindex="-1" role="dialog" aria-labelledby="CartModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CartModalTitle">Thông tin người đặt hàng</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Mời quý khách để lại thông tin, chúng tôi sẽ liên hệ lại ngay để tư vấn cho quý khách!</p>
        <form>
          <div class="form-group">
            <label for="ord-name" class="col-form-label">Họ và tên:</label>
            <input type="text" class="form-control" name="ord_name" id="ord-name">
          </div>
          <div class="form-group">
            <label for="ord-email" class="col-form-label">Email:</label>
            <input type="email" class="form-control" name="ord_email" id="ord-email">
          </div>
          <div class="form-group">
            <label for="ord-phone" class="col-form-label">Điện thoại:</label>
            <input type="text" class="form-control" name="ord_phone" id="ord-phone">
          </div>
          <div class="form-group">
            <label for="ord-content"  class="col-form-label">Ghi chú:</label>
            <textarea class="form-control" id="ord-content" name="ord_content"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary t-btn-order" data-id="{{ $event->id }}">Gửi</button>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
    <script>
      $(document).on('click','.btn-guinhanxet', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            let rat_title = $('#rat_title').val();
            let rat_star = $('#rat_star').val();
            let rat_content = $('#rat_content').val();
            let event_id = '{{ $id }}';
            $(__this).prop('disabled', true);
            if (rat_star == 0 || rat_star == null) {
                toastr.warning('Vui lòng chọn số sao bạn đánh giá!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (rat_title == '' || rat_title == null) {
              toastr.warning('Tiêu đề không được để trống!');
              $(__this).prop('disabled', false);
              return false;
            } 
            if (rat_content == '' || rat_content == null) {
                toastr.warning('Nội dung không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                rat_title : rat_title,
                rat_star : rat_star,
                rat_content : rat_content,
                event_id : event_id
            }
            let url = @json(route('dashboard-creat-rating'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
              console.log(msg);
                if (msg.type == 1) {
                    toastr.success(msg.mess);
                 
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
              if(jqXHR.responseJSON.errors.rat_title) {
                toastr.warning(jqXHR.responseJSON.errors.rat_title);
              }
              if(jqXHR.responseJSON.errors.rat_star) {
                toastr.warning(jqXHR.responseJSON.errors.rat_star);
              }
              if(jqXHR.responseJSON.errors.rat_content) {
                toastr.warning(jqXHR.responseJSON.errors.rat_content);
              }
              
            });
        })
      $(document).on('click','.t-btn-order', function (e) {
            e.preventDefault();
            toastr.clear();
            toastr.options = {
                "closeButton": true,
                "timeOut": "5000",
                "positionClass": "toast-bottom-right"
            }
            const __this = this;
            let ord_name = $('#ord-name').val();
            let ord_email = $('#ord-email').val();
            let ord_phone = $('#ord-phone').val();
            let ord_content = $('#ord-content').val();
            let event_id = '{{ $id }}';
            $(__this).prop('disabled', true);
            if (ord_name == 0 || ord_name == null) {
                toastr.warning('Họ và tên không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            if (ord_email == '' || ord_email == null) {
              toastr.warning('Email không được để trống!');
              $(__this).prop('disabled', false);
              return false;
            } 
            if (ord_phone == '' || ord_phone == null) {
                toastr.warning('Số điện thoại không được để trống!');
                $(__this).prop('disabled', false);
                return false;
            } 
            
            const __token = $('meta[name="__token"]').attr('content');
            data_ = {
                _token: __token,
                ord_name : ord_name,
                ord_email : ord_email,
                ord_phone : ord_phone,
                ord_content : ord_content,
                event_id : event_id
            }
            let url = @json(route('dashboard-creat-order'));
            let request = $.ajax({
            url: url,
            type: "POST",
            data: data_,
            dataType: "json"
            });
            request.done(function (msg) {
              console.log(msg);
                if (msg.type == 1) {
                    toastr.success(msg.mess);
                    $('#CartModal').modal('toggle');
                    $(__this).prop('disabled', false);
                }else {
                    toastr.warning(msg.mess);
                    $(__this).prop('disabled', false);
                }
                return false;
            });

            request.fail(function (jqXHR, textStatus) {
              if(jqXHR.responseJSON.errors.ord_name) {
                toastr.warning(jqXHR.responseJSON.errors.ord_name);
              }
              if(jqXHR.responseJSON.errors.ord_email) {
                toastr.warning(jqXHR.responseJSON.errors.ord_email);
              }
              if(jqXHR.responseJSON.errors.ord_phone) {
                toastr.warning(jqXHR.responseJSON.errors.ord_phone);
              }
              
            });
        })

    </script>
@endpush