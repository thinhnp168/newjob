@extends('client.layout.index')
@section('title', 'Chi tiết sự kiện')
@section('content')
<div class="container-fluid bgr-efefef pt-5">
    <div class="row">
      <div class="container">
        <ul class="nav nav-tabs justify-content-center t-home-nav-tabs-sukien" id="" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="sapdienra-tab" data-toggle="tab" href="#sapdienra" role="tab" aria-controls="sapdienra" aria-selected="false"><span>Sắp diễn ra</span> </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="dangdienra-tab" data-toggle="tab" href="#dangdienra" role="tab" aria-controls="dangdienra" aria-selected="true"><span>Đang diễn ra</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="moidang-tab" data-toggle="tab" href="#moidang" role="tab" aria-controls="moidang" aria-selected="false"><span>Mới đăng</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="mienphi-tab" data-toggle="tab" href="#mienphi" role="tab" aria-controls="mienphi" aria-selected="true"><span>Miễn phí</span></a>
          </li>
        </ul>
        <div class="tab-content" id="">
          <div class="tab-pane fade  " id="dangdienra" role="tabpanel" aria-labelledby="dangdienra-tab">
            <div class="row">
              @foreach ($eventDangdienra as $item)
                <div class="col-12 col-lg-3 col-sm-6 mb-4">
                  <div class="card t-card-khoahoc">
                    <figure>
                    <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                    </figure>
                    <div class="card-body">
                      <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                      <div class="row">
                        <div class="col-6 pr-0">
                          <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                        </div>
                        <div class="col-6 pr-0">
                          <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                        </div>
                      </div>
                      <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                      <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type ? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                      <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                      {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                    </div>
                  </div>
                </div> 
              @endforeach
            </div>
          </div>
          <div class="tab-pane fade  " id="mienphi" role="tabpanel" aria-labelledby="mienphi-tab">
            <div class="row">
              @foreach ($eventFree as $item)
                <div class="col-12 col-lg-3 col-sm-6 mb-4">
                  <div class="card t-card-khoahoc">
                    <figure>
                    <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                    </figure>
                    <div class="card-body">
                      <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                      <div class="row">
                        <div class="col-6 pr-0">
                          <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                        </div>
                        <div class="col-6 pr-0">
                          <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                        </div>
                      </div>
                      <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                      <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                      <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                      {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                    </div>
                  </div>
                </div> 
              @endforeach
            </div>
          </div>
          <div class="tab-pane fade" id="moidang" role="tabpanel" aria-labelledby="moidang-tab">
            <div class="row">
              @foreach ($eventNewCreat as $item)
                <div class="col-12 col-lg-3 col-sm-6 mb-4">
                  <div class="card t-card-khoahoc">
                    <figure>
                    <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                    </figure>
                    <div class="card-body">
                      <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                      <div class="row">
                        <div class="col-6 pr-0">
                          <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                        </div>
                        <div class="col-6 pr-0">
                          <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                        </div>
                      </div>
                      <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                      <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                      <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                      {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                    </div>
                  </div>
                </div> 
              @endforeach
            </div>
          </div>
          <div class="tab-pane fade show active" id="sapdienra" role="tabpanel" aria-labelledby="sapdienra-tab">
            <div class="row">
              @foreach ($eventSapdienra as $item)
                <div class="col-12 col-lg-3 col-sm-6 mb-4">
                  <div class="card t-card-khoahoc">
                    <figure>
                    <img src="{{ $item->eve_image }}" loading="lazy" class="card-img-top" alt="{{ $item->eve_name }}">
                    </figure>
                    <div class="card-body">
                      <a href=""><h5 class="card-title">{{ $item->eve_name }}</h5></a>
                      <div class="row">
                        <div class="col-6 pr-0">
                          <p class="border-right-calendar"><span class="icon icon-calendar"></span>{{ $item->eve_start_date  }}</p>
                        </div>
                        <div class="col-6 pr-0">
                          <p></span>{{ $item->eve_start_hour }} - {{ $item->eve_end_hour }}</p>
                        </div>
                      </div>
                      <p class="card-text"><span class="icon icon-ticket"></span>{{ $item->eve_price ? number_format($item->eve_price, 0, '', '.') .'VND' : 'Miễn phí' }} </p>
                      <p class="card-text"><span class="icon icon-place"></span> {!! $item->eve_address_type? '<span class="icon icon-online"></span>' : $item->eve_address !!} </p>
                      <a href="{{ route('dashboard-event-detail', [$item->eve_slug, $item->id]) }}" class="btn btn-primary btn-chitietsukien">Chi tiết sự kiện</a>
                      {{-- <p class="card-text text-center text-uppercase"><a href="" class="btn "><span class="icon icon-bell"></span> Nhắc tôi</a></p> --}}
                    </div>
                  </div>
                </div> 
              @endforeach
                
              
            </div>
          </div>
        </div>
        
      </div>
     
    </div>
  </div>
@endsection
@push('scripts')
@endpush