<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->name('home');
Route::get('/gioi-thieu.html', 'PhoneController@about')->name('about');
Route::post('/gioi-thieu.html', 'PhoneController@creatPhone')->name('client-create-phone');
Route::get('/{slug}-ctsk{id}.html', 'EventController@detail')->name('dashboard-event-detail')->where('id', '[0-9]+')->where('slug', '[a-zA-Z0-9-_]+');
Route::get('/danh-sach-tin-tuc.html', 'PostController@category')->name('category');
Route::get('/{slug}-cttt{id}.html', 'PostController@postDetail')->name('post-detail')->where('id', '[0-9]+')->where('slug', '[a-zA-Z0-9-_]+');
Route::post('/them-binh-luan.html', 'PostController@creatComment')->name('dashboard-creat-comment');
Route::post('/them-danh-gia.html', 'EventController@creatRating')->name('dashboard-creat-rating');
Route::get('/lien-he.html', 'ContactController@formContact')->name('client-form-contact');
Route::post('/lien-he.html', 'ContactController@createContact')->name('client-create-contact');
Route::get('/danh-sach-su-kien.html', 'EventController@listEvents')->name('client-list-event');
Route::post('/gui-dat-hang.html', 'OrderController@addOrder')->name('dashboard-creat-order');

Route::group(['prefix' => 'user', 'middleware' => ['auth']], function () {
    Route::get('/quan-ly-su-kien-qlsk{id}.html', 'DashboardController@dashboard')->name('dashboard')->where('id', '[0-9]+');
    Route::get('/them-su-kien.html', 'DashboardController@formAddEvent')->name('dashboard-form-add-event');
    Route::get('/{slug}-delete{id}.html', 'DashboardController@deleteEvent')->name('dashboard-delete-event')->where('id', '[0-9]+')->where('slug', '[a-zA-Z0-9-_]+');
    Route::get('/{slug}-cssk{id}.html', 'DashboardController@formEditEvent')->name('dashboard-form-edit-event')->where('id', '[0-9]+')->where('slug', '[a-zA-Z0-9-_]+');
    Route::post('/{slug}-cssk{id}.html', 'DashboardController@editEvent')->name('dashboard-edit-event')->where('id', '[0-9]+')->where('slug', '[a-zA-Z0-9-_]+');
    Route::post('/them-su-kien.html', 'DashboardController@addEvent')->name('dashboard-add-event');
});
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['role:admin|Super Admin', 'auth']], function () {
    Route::get('/', function () {
        return view('admin.home.index');
    })->name('admin_home');


    Route::prefix('users')->group(function () {
        Route::get('/delete/{id}', 'UserController@deleteUser')->name('delete-user')->middleware('role:Super Admin');
        Route::post('/add', 'UserController@addUser')->name('add-user');
        Route::get('/add', 'UserController@formAddUser')->name('form-add-user');
        Route::get('/list', 'UserController@listUser')->name('list-user');
        Route::get('/edit/{id}', 'UserController@formEditUser')->name('form-edit-user')->middleware('role:Super Admin');
        Route::post('/edit/{id}', 'UserController@editUser')->name('edit-user')->middleware('role:Super Admin');
        Route::post('/change-info/{id?}', 'UserController@changeInfo')->name('change-info-user')->middleware('role:Super Admin');
    });
    Route::prefix('posts')->group(function () {
        Route::get('/delete/{id}', 'PostController@deletePost')->name('delete-post');
        Route::get('/list', 'PostController@listPost')->name('list-post');
        Route::post('/edit/{id}', 'PostController@editPost')->name('edit-post');
        Route::get('/edit/{id}', 'PostController@formEditPost')->name('form-edit-post');
        Route::get('/add', 'PostController@formAddPost')->name('form-add-post');
        Route::post('/add', 'PostController@addPost')->name('add-post');
        Route::post('/change-info/{id?}', 'PostController@changeInfo')->name('change-info-post');
    });
    Route::group(['prefix' => 'permission', 'middleware' => ['role:Super Admin']], function () {
        Route::get('/delete-role/{id}', 'PermissionController@deleteRole')->name('delete-role');
        Route::get('/delete-permission/{id}', 'PermissionController@deletePermission')->name('delete-permission');
        Route::get('/list-role', 'PermissionController@listRole')->name('list-role');
        Route::get('/edit-permission/{id}', 'PermissionController@formEditPermission')->name('form-edit-permission');
        Route::post('/edit-permission/{id}', 'PermissionController@editPermission')->name('edit-permission');
        Route::get('/edit-role/{id}', 'PermissionController@formEditRole')->name('form-edit-role');
        Route::post('/edit-role/{id}', 'PermissionController@editRole')->name('edit-role');
        Route::get('/add-role', 'PermissionController@formAddRole')->name('form-add-role');
        Route::post('/add-role', 'PermissionController@addRole')->name('add-role');
        Route::get('/list-permission', 'PermissionController@listPermission')->name('list-permission');
        Route::get('/add-permission', 'PermissionController@formAddPermission')->name('form-add-permission');
        Route::post('/add-permission', 'PermissionController@addPermission')->name('add-permission');
        Route::get('/give-permission-to-role/{id}', 'PermissionController@formGivePermissionToRole')->name('form-give-permission-to-role');
        Route::post('/give-permission-to-role/{id}', 'PermissionController@givePermissionToRole')->name('give-permission-to-role');
        Route::get('/give-role-to-user/{id}', 'PermissionController@formGiveRoleToUser')->name('form-give-role-to-user');
        Route::post('/give-role-to-user/{id}', 'PermissionController@giveRoleToUser')->name('give-role-to-user');
    });
    Route::prefix('event')->group(function () {
        Route::get('/list/{id}', 'EventController@listEventOfUser')->name('list-event-of-user');
        Route::get('/list', 'EventController@listEvent')->name('list-event');
        Route::get('/add', 'EventController@formAddEvent')->name('form-add-event');
        Route::get('/edit/{id}', 'EventController@formEditEvent')->name('form-edit-event');
        Route::get('/delete/{id}', 'EventController@deleteEvent')->name('delete-event');
        Route::post('/edit/{id}', 'EventController@editEvent')->name('edit-event');
        Route::post('/add', 'EventController@addEvent')->name('add-event');
        Route::post('/change-info/{id?}', 'EventController@changeInfo')->name('change-info-event');
        Route::post('/change-value/{id?}', 'EventController@changeValue')->name('change-value-event');
    });
    Route::prefix('phone')->group(function () {
        Route::get('/list', 'PhoneController@listPhone')->name('list-phone');
    });
    Route::prefix('contact')->group(function () {
        Route::get('/list', 'ContactController@listContact')->name('list-contact');
        Route::get('/view/{id}', 'ContactController@viewContact')->name('view-contact');
        Route::post('/change-info/{id?}', 'ContactController@changeInfo')->name('change-info-contact');
    });
    Route::prefix('comment')->group(function () {
        Route::get('/list/{id}', 'CommentController@listCommentOfPosst')->name('list-comment-of-post');
        Route::get('/list', 'CommentController@listComment')->name('list-comment');
        Route::get('/delete/{id}', 'CommentController@deleteComment')->name('delete-comment');
        Route::post('/change-info/{id?}', 'CommentController@changeInfo')->name('change-info-comment');
    });
    Route::prefix('config')->group(function () {
        Route::get('/config', 'ConfigController@formAddConfig')->name('form-add-config');
        Route::post('/config', 'ConfigController@addConfig')->name('add-config');
    });
    Route::prefix('slider')->group(function () {
        Route::post('/change-info-position/{id?}', 'SliderController@changeInfo')->name('change-info-position');
        Route::get('/delete-position/{id}', 'SliderController@deletePosition')->name('delete-position');
        Route::get('/list-position', 'SliderController@listPosition')->name('list-position');
        Route::get('/edit-position/{id}', 'SliderController@formEditPosition')->name('form-edit-position');
        Route::post('/edit-position/{id}', 'SliderController@editPosition')->name('edit-position');
        Route::get('/add-position', 'SliderController@formAddPosition')->name('form-add-position');
        Route::post('/add-position', 'SliderController@addPosition')->name('add-position');
        Route::get('/add-slider', 'SliderController@formAddSlider')->name('form-add-slider');
        Route::get('/list-slider', 'SliderController@listSlider')->name('list-slider');
        Route::post('/add-slider', 'SliderController@addSlider')->name('add-slider');
        Route::post('/change-info-slider/{id?}', 'SliderController@changeInfoSlider')->name('change-info-slider');
        Route::post('/edit-slider/{id}', 'SliderController@editSlider')->name('edit-slider');
        Route::get('/edit-slider/{id}', 'SliderController@formEditSlider')->name('form-edit-slider');
        Route::get('/delete-slider/{id}', 'SliderController@deleteSlider')->name('delete-slider');
    });
});

// Auth::routes();
Route::get('/dang-xuat.html', 'Auth\LoginController@logout')->name('logOut');
Route::get('/dang-nhap.html', 'Auth\LoginController@store')->name('showLogin');
Route::post('/dang-nhap.html', 'Auth\LoginController@authenticate')->name('login');
Route::get('/dang-ky.html', 'Auth\RegisterController@store')->name('showRegister');
Route::post('/dang-ky.html', 'Auth\RegisterController@register')->name('register');
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});