<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pts_name' => 'required|max:255',
            'pts_image' => 'required',
            'pts_description' => 'required',
            'pts_seo_description' => 'required',
            'pts_seo_keyword' => 'required',
            'pts_content' => 'required',
            'pts_seo_title' => 'required|max:255',
        ];
    }
    public function messages()
    {
        return [
            'pts_name.required' => 'Tên không được để trống',
            'pts_description.required' => 'Mổ tả không được để trống',
            'pts_seo_description.required' => 'Description không được để trống',
            'pts_seo_keyword.required' => 'Keyword không được để trống',
            'pts_name.max' => 'Tên không được nhiều hơn 255 ký tự',
            'pts_image.required' => 'Ảnh đại diện không được để trống',
            'pts_seo_title.required' => 'Title không được để trống',
            'pts_seo_title.max' => 'Title không được nhiều hơn 255 ký tự',
            'pts_content.required' => 'Nội dung không được để trống',
        ];
    }
}
