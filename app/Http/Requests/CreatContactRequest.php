<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cct_name' => 'required:max:255',
            'cct_email' => 'required|email'
        ];
    }
    public function messages()
    {
        return [
            'cct_name.required' => 'Tên không được để trống',
            'cct_name.max' => 'Tên không được để quá 255 ký tự',
            'cct_email.required' => 'Email không được để trống',
            'cct_email.email' => 'Email không đúng định dạng!'
        ];
    }
}
