<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cmt_name' => 'required:max:255',
            'cmt_content' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'cmt_name.required' => 'Tên không được để trống',
            'cmt_name.max' => 'Tên không được để quá 255 ký tự',
            'cmt_content.required' => 'Nội dung không được để trống',
        ];
    }
}
