<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreatPhoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pho_phone' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/'
        ];
        
    }
    public function messages()
    {
        return [
            'pho_phone.required' => 'Số điện thoại không được để trống',
            'pho_phone.regex' => 'Số điện thoại không đúng định dạng'
        ];
    }
}
