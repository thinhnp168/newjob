<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CreatUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->all());
        return [
            'urs_name' => 'required:max:255',
            'urs_email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:32',
            'urs_phone' => 'required|regex:/^[84 0][3 5 7 8 9][0-9]{8}$/',
        ];
    }
    public function messages()
    {
        return [
            'urs_name.required' => 'Tên không được để trống',
            'urs_name.max' => 'Tên không được để quá 255 ký tự',
            'urs_email.required' => 'Email không được để trống',
            'urs_email.email' => 'Email không đúng định dạng',
            'urs_email.unique' => 'Email đã được sử dụng',
            'password.required' => 'Mật khẩu không được để trống',
            'password.min' => 'Mật khẩu không được ít hơn 6 ký tự',
            'password.max' => 'Mật khẩu không được nhiều hơn 31 ký tự',
            'urs_phone.required' => 'Số điện thoại không được để trống',
            'urs_phone.regex' => 'Số điện thoại không đúng định dạng'
        ];
    }
}
