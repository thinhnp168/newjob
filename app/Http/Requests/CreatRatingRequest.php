<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rat_title' => 'required:max:255',
            'rat_star' => 'required',
            'rat_content' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'rat_title.required' => 'Tên không được để trống',
            'rat_title.max' => 'Tên không được để quá 255 ký tự',
            'rat_star.required' => 'Số sao không được để trống',
            'rat_content.required' => 'Nội dung không được để trống',
        ];
    }
}
