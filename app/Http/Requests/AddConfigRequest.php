<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'con_name' => 'required|max:255',
            'con_website' => 'required',
            'con_email' => 'required|email',
            'con_tagline' => 'required|max:255',
            'con_facebook' => 'required',
            'con_youtube' => 'required',
            'con_zalo' => 'required',
            'con_adress_hn' => 'required',
            'con_adress_hcm' => 'required',
            'con_adress' => 'required',
            'con_phone_hn' => 'required',
            'con_phone_hcm' => 'required',
            'con_map' => 'required',
            'con_logo_top' => 'required',
            'con_logo_top_small' => 'required',
            'con_logo_bot' => 'required',
            'con_seo_description' => 'required',
            'con_seo_keyword' => 'required',
            'con_seo_image' => 'required',
            'con_favicon' => 'required',
            'con_seo_title' => 'required|max:255'
        ];
    }
    public function messages()
    {
        return [
            'con_name.required' => 'Tên không được để trống',
            'con_name.max' => 'Tên không được nhiều hơn 255 ký tự',
            'con_website.required' => 'Địa chỉ website không được để trống',
            'con_email.required' => 'Email không được để trống',
            'con_email.email' => 'Email không đúng định dạng',
            'con_tagline.required' => 'Tagline không được để trống',
            'con_email.max' => 'Tagline không được nhiều hơn 255 ký tự',
            'con_facebook.required' => 'Địa chỉ facebook không được để trống',
            'con_youtube.required' => 'Địa chỉ youtube không được để trống',
            'con_zalo.required' => 'Địa chỉ zalo không được để trống',
            'con_adress_hn.required' => 'Địa chỉ văn phòng Hà Nội không được để trống',
            'con_adress_hcm.required' => 'Địa chỉ văn phòng Hồ Chí Minh không được để trống',
            'con_adress.required' => 'Địa chỉ trụ sở không được để trống',
            'con_phone_hn.required' => 'Số điện thoại văn phòng Hà Nội không được để trống',
            'con_phone_hcm.required' => 'Số điện thoại văn phòng Hồ Chí Minh không được để trống',
            'con_map.required' => 'Iframe bản đồ không được để trống',
            'con_logo_top.required' => 'Logo top không được để trống',
            'con_logo_top_small.required' => 'Logo top nhỏ không được để trống',
            'con_logo_bot.required' => 'Logo bottom không được để trống',
            'con_seo_title.required' => 'Title seo không được để trống',
            'con_seo_title.max' => 'Title seo không được nhiều hơn 255 ký tự',
            'con_seo_description.required' => 'Desription seo seo không được để trống',
            'con_seo_keyword.required' => 'Keyword seo không được để trống',
            'con_seo_image.required' => 'Image seo không được để trống',
            'con_favicon.required' => 'Favicon không được để trống',
            
        ];
    }
}
