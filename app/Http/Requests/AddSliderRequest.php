<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sli_title' => 'required:max:255',
            'sli_image' => 'required',
            'sli_link' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'sli_title.required' => 'Tên không được để trống',
            'sli_title.required' => 'Tên không được quá 255 ký tự',
            'sli_image.required' => 'Ảnh không được để trống',
            'sli_link.required' => 'Đường dẫn không được để trống'
        ];
    }
}
