<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ord_name' => 'required',
            'ord_email' => 'required|email',
            'ord_phone' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'ord_name.required' => 'Họ và tên không được để trống',
            'ord_email.required' => 'Email không được để trống',
            'ord_email.email' => 'Email không đúng định dạng',
            'ord_phone.required' => 'Số điện thoại không được để trống'
        ];
    }
}