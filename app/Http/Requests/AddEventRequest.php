<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddEventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eve_name' => 'required|max:255',
            'eve_image' => 'required',
            'eve_avatar' => 'required',
            'eve_description' => 'required',
            'eve_start_date' => 'required',
            'eve_end_date' => 'required',
            'eve_introduce' => 'required',
            'eve_content' => 'required',
            'eve_lecturer' => 'required',
            'eve_seo_description' => 'required',
            'eve_seo_keyword' => 'required',
            'eve_seo_title' => 'required|max:255'
        ];
    }
    public function messages()
    {
        return [
            'eve_name.required' => 'Tên không được để trống',
            'eve_name.max' => 'Tên không được nhiều hơn 255 ký tự',
            'eve_image.required' => 'Ảnh bìa không được để trống',
            'eve_avatar.required' => 'Ảnh đại diện không được để trống',
            'eve_description.required' => 'Mô tả không được để trống',
            'eve_start_date.required' => 'Ngày bắt đầu không được để trống',
            'eve_end_date.required' => 'Ngày kết thúc không được để trống',
            'eve_introduce.required' => 'Giới thiệu không được để trống',
            'eve_content.required' => 'Nội dung không được để trống',
            'eve_lecturer.required' => 'Thông tin giảng viên không được để trống',
            'eve_seo_description.required' => 'Description không được để trống',
            'eve_seo_keyword.required' => 'Keyword không được để trống',
            'eve_seo_title.required' => 'Title không được để trống',
            'eve_seo_title.max' => 'Title không được nhiều hơn 255 ký tự'
        ];
    }
}
