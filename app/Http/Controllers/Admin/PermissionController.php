<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddPermissionRequest;
use App\Http\Requests\AddRoleRequest;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    protected $userInterface;
    function __construct(UserInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }
    public function formAddRole()
    {
        return view('admin.permission.add-role');
    }
    public function addRole (AddRoleRequest $request)
    {
        $addRole = new Role;
        $addRole->name = $request->name;
        $addRole->save();
        return redirect()->route('list-role')->with('success_mess', 'Thêm thành công!');
    }
    public function formAddPermission()
    {
        return view('admin.permission.add-permission');
    }
    public function addPermission (AddPermissionRequest $request)
    {
        $addPermission = new Permission;
        $addPermission->name = $request->name;
        $addPermission->save();
        return redirect()->route('list-permission')->with('success_mess', 'Thêm thành công!');
    }
    public function listRole()
    {
        $listRole = Role::all();
        return view('admin.permission.list-role', compact('listRole'));
    }
    public function listPermission()
    {
        $listPermission = Permission::all();
        return view('admin.permission.list-permission', compact('listPermission'));
    }
    public function formGivePermissionToRole($id)
    {
        $role = Role::findOrFail($id);
        $allPermission = Permission::all();
        $permissionsInRole = [];
        foreach ($role->getAllPermissions() as $key => $value) {
            $permissionsInRole[] = $value->id;
        }
        return view('admin.permission.give-permission-to-role', compact('role', 'allPermission', 'permissionsInRole'));
    }
    public function givePermissionToRole($id, Request $request)
    {
        $role = Role::findOrFail($id);
        $permissions = $request->permission;
        $result = $role->syncPermissions($permissions);
        if ($result) {
            return redirect()->route('list-role')->with('success_mess', 'Gán quyền thành công!');
        } else {
            return redirect()->back()->with('error_mess', 'Có lỗi xảy ra, vui lòng thử lại!');
        }
    }
    public function formGiveRoleToUser($id)
    {
        $user = $this->userInterface->findOrFail($id);
        $allRole = Role::all();
        $roleName = [];
        foreach ($user->getRoleNames() as $key => $value) {
            $roleName[] = $value;
        }
        
        return view('admin.permission.give-role-to-user', compact('user', 'allRole', 'roleName'));
    }
    public function giveRoleToUser($id, Request $request)
    {
        $user = $this->userInterface->findOrFail($id);
        $role = $request->role;
        $result = $user->syncRoles($role);
        if($result) {
            return redirect()->route('list-user')->with('success_mess', 'Gán vai trò thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi xảy ra, vui lòng thử lại!');
        }
    }
    public function formEditRole($id)
    {
        $role = Role::findOrFail($id);
        return view('admin.permission.edit-role', compact('role'));
    }
    public function editRole($id, AddRoleRequest $request)
    {
        $role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->save();
        return redirect()->route('list-role')->with('success_mess', 'Sửa vài trò thành công!');
    }
    public function deleteRole($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect()->route('list-role')->with('success_mess', 'Xoá vài trò thành công!');
    }
    public function formEditPermission($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permission.edit-permission', compact('permission'));
    }
    public function editPermission($id, AddPermissionRequest $request)
    {
        $permission = Permission::findOrFail($id);
        $permission->name = $request->name;
        $permission->save();
        return redirect()->route('list-permission')->with('success_mess', 'Sửa quyền thành công!');
    }
    public function deletePermission($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        return redirect()->route('list-permission')->with('success_mess', 'Xoá quyền thành công!');
    }
}
