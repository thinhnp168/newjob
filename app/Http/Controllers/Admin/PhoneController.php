<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Phone\PhoneInterface;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    protected $phoneInterface;
    public function __construct(PhoneInterface $phoneInterface)
    {
        $this->phoneInterface = $phoneInterface;
    }
    public function listPhone()
    {
        $listPhones = $this->phoneInterface->getAll();
        return view('admin.phone.list', compact('listPhones'));
    }
    
}