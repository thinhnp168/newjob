<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddPositionRequest;
use App\Http\Requests\AddSliderRequest;
use App\Repositories\Position\PositionInterface;
use App\Repositories\Slider\SliderInterface;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    protected $positionInterface;
    protected $sliderInterface;

    public function __construct(PositionInterface $positionInterface, SliderInterface $sliderInterface)
    {
        $this->positionInterface = $positionInterface;
        $this->sliderInterface = $sliderInterface;
    }
    public function formAddPosition()
    {
       return view('admin.slider.add-position');
    }
    public function addPosition(AddPositionRequest  $request)
    {
        $param = [
            'pos_name' => $request->pos_name,
            'pos_status' => $request->pos_status,
        ];
        $result = $this->positionInterface->create($param);
        if($result) {
            return redirect()->route('list-position')->with('success_mess', 'Thêm mới thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function listPosition()
    {
        $listPositions = $this->positionInterface->getAll();
        return view('admin.slider.list-position', compact('listPositions'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->positionInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditPosition($id)
    {
        $position = $this->positionInterface->findOrFail($id);
        return view('admin.slider.edit-position', compact('position'));
    }
    public function editPosition($id, AddPositionRequest $request)
    {
        $param = [
            'pos_name' => $request->pos_name,
            'pos_status' => $request->pos_status,
        ];
        $result = $this->positionInterface->update($id, $param);
        if($result) {
            return redirect()->route('list-position')->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function deletePosition($id)
    {
        $result = $this->positionInterface->delete($id);
        if($result) {
            return redirect()->route('list-position')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function formAddSlider()
    {
        $position = $this->positionInterface->getAll();
        return view('admin.slider.add-slider', compact('position'));
    }
    public function addSlider(AddSliderRequest $request)
    {
        $param = [
            'sli_title' => $request->sli_title,
            'sli_image' => $request->sli_image,
            'sli_link' => $request->sli_link,
            'sli_content' => $request->sli_content,
            'position_id' => $request->position_id,
            'sli_status' => $request->sli_status
        ];
        $result = $this->sliderInterface->create($param);
        if($result) {
            return redirect()->route('list-slider')->with('success_mess', 'Thêm mới thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function listSlider()
    {
        $listSliders = $this->sliderInterface->getAll();
        return view('admin.slider.list-slider', compact('listSliders'));
    }
    public function changeInfoSlider($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->sliderInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditSlider($id)
    {
        $position = $this->positionInterface->getAll();
        $slider = $this->sliderInterface->findOrFail($id);
        return view('admin.slider.edit-slider', compact('slider', 'position'));
    }
    public function editSlider($id, AddSliderRequest $request)
    {
        $param = [
            'sli_title' => $request->sli_title,
            'sli_image' => $request->sli_image,
            'sli_link' => $request->sli_link,
            'sli_content' => $request->sli_content,
            'position_id' => $request->position_id,
            'sli_status' => $request->sli_status
        ];
        $result = $this->sliderInterface->update($id, $param);
        if($result) {
            return redirect()->route('list-slider')->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function deleteSlider($id)
    {
        $result = $this->sliderInterface->delete($id);
        if($result) {
            return redirect()->route('list-slider')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}
