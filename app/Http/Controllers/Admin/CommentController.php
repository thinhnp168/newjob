<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Comment\CommentInterface;
use App\Repositories\Post\PostInterface;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    protected $commentInterface;
    protected $postInterface;
    public function __construct(CommentInterface $commentInterface, PostInterface $postInterface)
    {
        $this->commentInterface = $commentInterface;
        $this->postInterface = $postInterface;
    }
    public function listComment()
    {
        $listComments = $this->commentInterface->getAll();
        return view('admin.comment.list', compact('listComments'));
    }
    public function listCommentOfPosst($id)
    {
        $post = $this->postInterface->findOrFail($id);
        $where = [
            'post_id' => $id
        ];
        $listComments = $this->commentInterface->getAllConditionWith($where, []);
        return view('admin.comment.list', compact('listComments'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->commentInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function deleteComment($id)
    {
        $result = $this->commentInterface->delete($id);
        if($result) {
            return redirect()->route('list-comment')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}