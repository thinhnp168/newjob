<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contact\ContactInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $contactInterface;
    public function __construct(ContactInterface $contactInterface)
    {
        $this->contactInterface = $contactInterface;
    }
    public function listContact()
    {
        $listContacts = $this->contactInterface->getAll();
        return view('admin.contact.list', compact('listContacts'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->contactInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function viewContact($id)
    {
        $contact = $this->contactInterface->findOrFail($id);
        return view('admin.contact.view', compact('contact'));
    }
}
