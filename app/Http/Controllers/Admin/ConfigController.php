<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddConfigRequest;
use App\Repositories\Config\ConfigInterface;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    protected $configInterface;
    public function __construct(ConfigInterface $configInterface)
    {
        $this->configInterface = $configInterface;
    }
    public function formAddConfig()
    {
        $config = $this->configInterface->getIdConditionWith(1, []);
        
        return view('admin.config.add', compact('config'));
    }
    public function addConfig(AddConfigRequest $request)
    {
        $param = [
            'con_name' => $request->con_name,
            'con_website' => $request->con_website,
            'con_email' => $request->con_email,
            'con_tagline' => $request->con_tagline,
            'con_facebook' => $request->con_facebook,
            'con_youtube' => $request->con_youtube,
            'con_zalo' => $request->con_zalo,
            'con_adress_hn' => $request->con_adress_hn,
            'con_adress_hcm' => $request->con_adress_hcm,
            'con_adress' => $request->con_adress,
            'con_phone_hn' => $request->con_phone_hn,
            'con_phone_hcm' => $request->con_phone_hcm,
            'con_map' => $request->con_map,
            'con_logo_top' => $request->con_logo_top,
            'con_favicon' => $request->con_favicon,
            'con_logo_top_small' => $request->con_logo_top_small,
            'con_seo_image' => $request->con_seo_image,
            'con_logo_bot' => $request->con_logo_bot,
            'con_seo_title' => $request->con_seo_title,
            'con_seo_description' => $request->con_seo_description,
            'con_seo_keyword' => $request->con_seo_keyword
        ];
        $config = $this->configInterface->getIdConditionWith(1, []);
        if($config) {
            $result = $this->configInterface->update(1, $param);
        }else {
            $result = $this->configInterface->create($param);
        }
        if($result) {
            return redirect()->back()->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}