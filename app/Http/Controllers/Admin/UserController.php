<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $userInterface;
    function __construct(UserInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }
    public function listUser()
    {
        $listUser = $this->userInterface->getAll();
        return view('admin.user.list', compact('listUser'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->userInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditUser($id)
    {
        $editUser = $this->userInterface->findOrFail($id);
        return view('admin.user.edit', compact('editUser'));
    }
    public function editUser($id, EditUserRequest $request)
    {
        $data = [];
        $data['urs_name'] =  $request->urs_name;
        $data['urs_phone'] =  $request->urs_phone;
        $data['urs_email'] =  $request->urs_email;
        if(!empty($request->password)) {
            $data['password'] =  Hash::make($request->password);
        }

        $editUser = $this->userInterface->update($id, $data);

        if($editUser) {
            return redirect()->route('list-user')->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function deleteUser($id)
    {
        $deleteUser= $this->userInterface->delete($id);
        if($deleteUser) {
            return redirect()->route('list-user')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function formAddUser()
    {
        return view('admin.user.add');
    }
    public function addUser(CreatUserRequest $request)
    {
        $data = [];
        $data['urs_name'] =  $request->urs_name;
        $data['urs_phone'] =  $request->urs_phone;
        $data['urs_email'] =  $request->urs_email;
        $data['password'] =  Hash::make($request->password);

        $createUser = $this->userInterface->saveUser($data);
        if ($createUser) 
        {
            return redirect()->route('list-user')->with('success_mess', 'Thêm mới thành công!');

        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}
