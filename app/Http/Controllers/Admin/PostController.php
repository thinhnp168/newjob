<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddPostRequest;
use App\Repositories\Post\PostInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    protected $postInterface;
    function __construct(PostInterface $postInterface)
    {
        $this->postInterface = $postInterface;
    }
    public function formAddPost()
    {
        return view('admin.post.add');
    }
    public function addPost(AddPostRequest $request)
    {
        $params = [
            'user_id'                   => auth()->user()->id,
            'pts_name'                  => $request->pts_name,
            'pts_image'                 => $request->pts_image,
            'pts_seo_title'             => $request->pts_seo_title,
            'pts_description'           => $request->pts_description,
            'pts_seo_description'       => $request->pts_seo_description,
            'pts_seo_keyword'           => $request->pts_seo_keyword,
            'pts_slug'                  => Str::slug($request->pts_name),
            'pts_content'               => $request->pts_content,
            'pts_status'                => $request->pts_status,
        ];
        $addPost = $this->postInterface->create($params);
        if($addPost) {
            return redirect()->route('list-post')->with('success_mess', 'Thêm mới thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function listPost()
    {
        $with = ['user'];
        $where = [];
        $listPosts = $this->postInterface->getAllConditionWith($where, $with);
        return view('admin.post.list', compact('listPosts'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->postInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditPost($id)
    {
        $editPost = $this->postInterface->findOrFail($id);
        if($editPost) {
            return view('admin.post.edit', compact('editPost'));
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function editPost($id, AddPostRequest $request)
    {
        $params = [
            'user_id'                   => auth()->user()->id,
            'pts_name'                  => $request->pts_name,
            'pts_image'                 => $request->pts_image,
            'pts_seo_title'             => $request->pts_seo_title,
            'pts_description'           => $request->pts_description,
            'pts_seo_description'       => $request->pts_seo_description,
            'pts_seo_keyword'           => $request->pts_seo_keyword,
            'pts_slug'                  => Str::slug($request->pts_name),
            'pts_content'               => $request->pts_content,
            'pts_status'                => $request->pts_status,
        ];
        $editPost = $this->postInterface->update($id, $params);
        if($editPost) {
            return redirect()->route('list-post')->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function deletePost($id)
    {
        $deletePost = $this->postInterface->delete($id);
        if($deletePost) {
            return redirect()->route('list-post')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}