<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddEventRequest;
use App\Repositories\Event\EventInterface;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EventController extends Controller
{
    protected $eventInterface;
    protected $userInterface;
    public function __construct(EventInterface $eventInterface, UserInterface $userInterface)
    {
        $this->eventInterface = $eventInterface;
        $this->userInterface = $userInterface;
    }
    public function formAddEvent()
    {
        return view('admin.event.add');
    }
    public function addEvent(Request $request)
    {
        $param = [
            'eve_name' => $request->eve_name,
            'eve_slug' => Str::slug($request->eve_name),
            'eve_image' => $request->eve_image,
            'eve_status' => $request->eve_status,
            'eve_avatar' => $request->eve_avatar,
            'eve_description' => $request->eve_description,
            'eve_start_date' => $request->eve_start_date,
            'eve_end_date' => $request->eve_end_date,
            'eve_end_hour' => $request->eve_end_hour,
            'eve_start_hour' => $request->eve_start_hour,
            'eve_price' => $request->eve_price == null ? 0 :  $request->eve_price,
            'eve_address_type' => $request->eve_address_type,
            'eve_address' => $request->eve_address,
            'eve_introduce' => $request->eve_introduce,
            'eve_content' => $request->eve_content,
            'eve_lecturer' => $request->eve_lecturer,
            'eve_is_hot' => $request->eve_is_hot,
            'eve_seo_description' => $request->eve_seo_description,
            'eve_seo_title' => $request->eve_seo_title,
            'eve_seo_keyword' => $request->eve_seo_keyword,
            'user_id' =>  auth()->user()->id
        ];
        $addEvent = $this->eventInterface->create($param);
        if($addEvent) {
            return redirect()->route('list-event')->with('success_mess', 'Thêm mới thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function listEventOfUser($id)
    {
        $user = $this->userInterface->findOrFail($id);
        $where = [
            'user_id' => $id
        ];
        $listEvents = $this->eventInterface->getAllConditionWith($where, []);
        return view('admin.event.list', compact('listEvents', 'user'));
    }
    public function listEvent()
    {
        $listEvents = $this->eventInterface->getAll();
        return view('admin.event.list', compact('listEvents'));
    }
    public function changeInfo($id, Request $request)
    {
        $type = $request->type;
        $changeInfo = $this->eventInterface->changeInfo($id, $request->all());
        if(!empty($changeInfo)) {
            if($changeInfo->$type == 1) {
                return response()->json([
                    'type' => 0,
                    'mess' => 'Thành công!'
                ]);
            }else {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Thành công!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function changeValue($id, Request $request)
    {
        $changeValue = $this->eventInterface->changeValue($id, $request->all());
        if($changeValue) {
            return response()->json([
                'type' => 1,
                'mess' => 'Thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function formEditEvent($id)
    {
        $event = $this->eventInterface->findOrFail($id);
        return view('admin.event.edit', compact('event'));
    }
    public function editEvent($id, AddEventRequest $request)
    {
        $param = [
            'eve_name' => $request->eve_name,
            'eve_slug' => Str::slug($request->eve_name),
            'eve_image' => $request->eve_image,
            'eve_status' => $request->eve_status,
            'eve_avatar' => $request->eve_avatar,
            'eve_description' => $request->eve_description,
            'eve_start_date' => $request->eve_start_date,
            'eve_end_date' => $request->eve_end_date,
            'eve_end_hour' => $request->eve_end_hour,
            'eve_start_hour' => $request->eve_start_hour,
            'eve_price' => $request->eve_price == null ? 0 :  $request->eve_price,
            'eve_address_type' => $request->eve_address_type,
            'eve_address' => $request->eve_address,
            'eve_introduce' => $request->eve_introduce,
            'eve_content' => $request->eve_content,
            'eve_lecturer' => $request->eve_lecturer,
            'eve_is_hot' => $request->eve_is_hot,
            'eve_seo_description' => $request->eve_seo_description,
            'eve_seo_title' => $request->eve_seo_title,
            'eve_seo_keyword' => $request->eve_seo_keyword
        ];
        $editEvent = $this->eventInterface->update($id, $param);
        if($editEvent) {
            return redirect()->route('list-event')->with('success_mess', 'Chỉnh sửa thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function deleteEvent($id)
    {
        $deleteEvent = $this->eventInterface->delete($id);
        if($deleteEvent) {
            return redirect()->route('list-event')->with('success_mess', 'Xoá thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}
