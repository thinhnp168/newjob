<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatContactRequest;
use App\Repositories\Contact\ContactInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $contactInterface;
    function __construct(ContactInterface $contactInterface)
    {
        $this->contactInterface = $contactInterface;
    }
    public function formContact()
    {
        return view('client.contact.contact');
    }
    public function createContact(CreatContactRequest $request)
    {
        $param = [
            'cct_name' => $request->cct_name,
            'cct_title' => $request->cct_title,
            'cct_email' => $request->cct_email,
            'cct_status' => 1,
            'cct_content' => $request->cct_content
        ];
        $addContact = $this->contactInterface->create($param);
        if($addContact) {
            return redirect()->back()->with('success_mess', 'Gửi thông tin thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}
