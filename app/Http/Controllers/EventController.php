<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatRatingRequest;
use App\Repositories\Event\EventInterface;
use App\Repositories\Rating\RatingInterface;
use Illuminate\Http\Request;
use App\Event;

class EventController extends Controller
{
    protected $eventInterface;
    protected $ratingInterface;
    public function __construct(EventInterface $eventInterface, RatingInterface $ratingInterface)
    {
        $this->eventInterface = $eventInterface;
        $this->ratingInterface = $ratingInterface;
    }
    public function detail($slug, $id)
    {
        $reration = ['rating'];
        $event = $this->eventInterface->getIdConditionWith($id, $reration);
        $event = $ratingEvent1 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1);
            }
        ])->first();
        $ratingEvent1 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1)->where('rat_star', 1);
            }
        ])->first();
        $ratingEvent2 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1)->where('rat_star', 2);
            }
        ])->first();
        $ratingEvent3 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1)->where('rat_star', 3);
            }
        ])->first();
        $ratingEvent4 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1)->where('rat_star', 4);
            }
        ])->first();
        $ratingEvent5 = Event::where('id', $id)->with([
            'rating' => function ($query) {
                $query->where('rat_status', 1)->where('rat_star', 5);
            }
        ])->first();

        if (count($ratingEvent1->rating) + count($ratingEvent2->rating) + count($ratingEvent3->rating) + count($ratingEvent4->rating) + count($ratingEvent5->rating)) {
            $avgStar =round((count($ratingEvent1->rating) + count($ratingEvent1->rating) * 2 + count($ratingEvent3->rating) * 3 + count($ratingEvent4->rating) * 4 + count($ratingEvent5->rating) * 5) / (count($ratingEvent1->rating) + count($ratingEvent2->rating) + count($ratingEvent3->rating) + count($ratingEvent4->rating) + count($ratingEvent5->rating)), 1);
        } else {
            $avgStar = 5;
        }
        $totalRate = count($ratingEvent1->rating) + count($ratingEvent2->rating) + count($ratingEvent3->rating) + count($ratingEvent4->rating) + count($ratingEvent5->rating);
        if($totalRate) {
            $totalRate1star = round((count($ratingEvent1->rating)*100)/$totalRate);
            $totalRate2star = round((count($ratingEvent2->rating)*100)/$totalRate);
            $totalRate3star = round((count($ratingEvent3->rating)*100)/$totalRate);
            $totalRate4star = round((count($ratingEvent4->rating)*100)/$totalRate);
            $totalRate5star = round((count($ratingEvent5->rating)*100)/$totalRate);
        }else {
            $totalRate1star = 0;
            $totalRate2star = 0;
            $totalRate3star = 0;
            $totalRate4star = 0;
            $totalRate5star = 0;
        }
        
        $eventRelated = $this->eventInterface->getAmountEventRandom(4);
        return view('client.event.event', compact('id', 'event', 'eventRelated', 'avgStar', 'totalRate' , 'totalRate1star', 'totalRate2star', 'totalRate3star', 'totalRate4star', 'totalRate5star'));
    }
    public function creatRating(CreatRatingRequest $request)
    {
        $param = [
            'rat_title' => $request->rat_title,
            'rat_star' => $request->rat_star,
            'rat_content' => $request->rat_content,
            'event_id' => $request->event_id,
            'rat_status' => 0
        ];
        $addRating = $this->ratingInterface->create($param);
        if ($addRating) {
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi đánh giá thành công!Đánh giá sẽ được xét duyệt trước khi hiển thị'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
    public function listEvents()
    {
        $eventSapdienra = $this->eventInterface->getEventComingOutSoon();
        $eventDangdienra = $this->eventInterface->getEventGoingOn();
        $eventNewCreat = $this->eventInterface->getEventNewCreat(8);
        $eventFree = $this->eventInterface->getEventFree();
        return view('client.event.list', compact('eventSapdienra', 'eventDangdienra', 'eventNewCreat', 'eventFree'));
    }
}