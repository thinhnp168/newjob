<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddEventRequest;
use App\Repositories\Event\EventInterface;
use App\Repositories\User\UserInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    protected $eventInterface;
    protected $userInterface;
    function __construct(EventInterface $eventInterface, UserInterface $userInterface)
    {
        $this->eventInterface = $eventInterface;
        $this->userInterface = $userInterface;
    }
    public function dashboard($id)
    {
        $user = $this->userInterface->findOrFail($id);
        $where = [
            'user_id' => $id
        ];
        $listEvents = $this->eventInterface->getAllConditionWith($where, []);
        return view('client.dashboard.dashboard', compact('listEvents', 'id'));
    }
    public function formAddEvent()
    {
        return view('client.dashboard.add');
    }
    public function addEvent(AddEventRequest $request)
    {
        $param = [
            'eve_name' => $request->eve_name,
            'eve_slug' => Str::slug($request->eve_name),
            'eve_image' => $request->eve_image,
            'eve_avatar' => $request->eve_avatar,
            'eve_description' => $request->eve_description,
            'eve_start_date' => $request->eve_start_date,
            'eve_end_date' => $request->eve_end_date,
            'eve_end_hour' => $request->eve_end_hour,
            'eve_start_hour' => $request->eve_start_hour,
            'eve_price' => $request->eve_price == null ? 0 :  $request->eve_price,
            'eve_address_type' => $request->eve_address_type,
            'eve_address' => $request->eve_address,
            'eve_introduce' => $request->eve_introduce,
            'eve_content' => $request->eve_content,
            'eve_lecturer' => $request->eve_lecturer,
            'eve_is_hot' => 0,
            'eve_status' => 0,
            'eve_seo_description' => $request->eve_seo_description,
            'eve_seo_title' => $request->eve_seo_title,
            'eve_seo_keyword' => $request->eve_seo_keyword,
            'user_id' =>  auth()->user()->id
        ];
        $addEvent = $this->eventInterface->create($param);
        if($addEvent) {
            return redirect()->route('dashboard', auth()->user()->id)->with('success_mess', 'Thêm mới thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
    public function formEditEvent($slug, $id)
    {
        $event = $this->eventInterface->findOrFail($id);
        if(Auth::user()->id == $event->user_id) {
            return view('client.dashboard.edit', compact('event'));
        }else {
            return redirect()->route('dashboard', auth()->user()->id)->with('error_mess', 'Bạn không có quyền chỉnh sửa sự kiện này!');
        }
        
    }
    public function editEvent($slug, $id, AddEventRequest $request)
    {
        
        $event = $this->eventInterface->findOrFail($id);
        if(Auth::user()->id == $event->user_id) {
            $param = [
                'eve_name' => $request->eve_name,
                'eve_slug' => Str::slug($request->eve_name),
                'eve_image' => $request->eve_image,
                'eve_avatar' => $request->eve_avatar,
                'eve_description' => $request->eve_description,
                'eve_start_date' => $request->eve_start_date,
                'eve_end_date' => $request->eve_end_date,
                'eve_end_hour' => $request->eve_end_hour,
                'eve_start_hour' => $request->eve_start_hour,
                'eve_price' => $request->eve_price == null ? 0 :  $request->eve_price,
                'eve_address_type' => $request->eve_address_type,
                'eve_address' => $request->eve_address,
                'eve_introduce' => $request->eve_introduce,
                'eve_content' => $request->eve_content,
                'eve_lecturer' => $request->eve_lecturer,
                'eve_status' => 0,
                'eve_seo_description' => $request->eve_seo_description,
                'eve_seo_title' => $request->eve_seo_title,
                'eve_seo_keyword' => $request->eve_seo_keyword
            ];
            $editEvent = $this->eventInterface->update($id, $param);
            if($editEvent) {
                return redirect()->route('dashboard', auth()->user()->id)->with('success_mess', 'Chỉnh sửa thành công!');
            }else {
                return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
            }
        }else {
            return redirect()->route('dashboard', auth()->user()->id)->with('error_mess', 'Bạn không có quyền chỉnh sửa sự kiện này!');
        }
    }
    public function deleteEvent($slug,$id)
    {
        $event = $this->eventInterface->findOrFail($id);
        if(Auth::user()->id == $event->user_id) {
            $deleteEvent = $this->eventInterface->delete($id);
            if($deleteEvent) {
                return redirect()->route('dashboard', auth()->user()->id)->with('success_mess', 'Xoá thành công!');
            }else {
                return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
            }
        }else {
            return redirect()->route('dashboard', auth()->user()->id)->with('error_mess', 'Bạn không có quyền xoá sự kiện này!');
        }
    }
}
