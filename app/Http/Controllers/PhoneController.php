<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatPhoneRequest;
use App\Repositories\Phone\PhoneInterface;
use App\Repositories\Slider\SliderInterface;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    protected $phoneInterface;
    protected $sliderInterface;
    public function __construct(PhoneInterface $phoneInterface, SliderInterface $sliderInterface)
    {
        $this->phoneInterface = $phoneInterface;
        $this->sliderInterface = $sliderInterface;
    }
    public function about()
    {
        $feedback = [
            'position_id' => 5,
            'sli_status' => 1
        ];
        $sliderFeedBack = $this->sliderInterface->getAllByCondition($feedback);
        return view('client.about.about', compact('sliderFeedBack'));
    }
    public function creatPhone(CreatPhoneRequest $request)
    {
        $param = [
            'pho_phone' => $request->pho_phone
        ];
        $creatPhone = $this->phoneInterface->create($param);
        if($creatPhone) {
            return redirect()->back()->with('success_mess', 'Gửi thông tin thành công!');
        }else {
            return redirect()->back()->with('error_mess', 'Có lỗi sảy ra, vui lòng thử lại!');
        }
    }
}
