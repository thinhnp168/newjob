<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatCommentRequest;
use App\Repositories\Comment\CommentInterface;
use App\Repositories\Post\PostInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected $postInterface;
    protected $commentInterface;

    public function __construct(PostInterface $postInterface, CommentInterface $commentInterface)
    {
        $this->postInterface = $postInterface;
        $this->commentInterface = $commentInterface;
    }
    public function postDetail($slug, $id)
    {
        $relative = ['comment' => function($query) {
            $query->where('cmt_status',1);
        }];
        $postDetail = $this->postInterface->getIdConditionWith($id, $relative); 
        $listnewPosts = $this->postInterface->getAmountPostNewest(6);
        // dd($postDetail);
        $postRelated = $this->postInterface->getAmountPostRandom(6);
        return view('client.post.detail', compact('id', 'postDetail', 'listnewPosts', 'postRelated'));
    }
    public function creatComment(CreatCommentRequest $request)
    {
        if(Auth::check()) {
            $param = [
                'cmt_name' => $request->cmt_name,
                'cmt_email' => $request->cmt_email,
                'cmt_content' => $request->cmt_content,
                'post_id' => $request->post_id,
                'parent_id' => $request->parent_id,
                'user_id' => Auth::user()->id,
                'cmt_status' => 0
            ];
            $addComment = $this->commentInterface->create($param);
            if($addComment) {
                return response()->json([
                    'type' => 1,
                    'mess' => 'Gửi bình luận thành công! Bình luận sẽ được xét duyệt trước khi hiển thị'
                ]);
            }else {
                return response()->json([
                    'type' => 2,
                    'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
                ]);
            }
        }else {
            return response()->json([
                'type' => 2,
                'mess' => 'Vui lòng đăng nhập để thực hiện chức năng này!'
            ]);
        }
        
    }
    public function category()
    {
        $listPosts = $this->postInterface->paginate(10, []);
        $listnewPosts = $this->postInterface->getAmountPostNewest(6);
        return view('client.post.category', compact('listPosts', 'listnewPosts'));
    }
}
