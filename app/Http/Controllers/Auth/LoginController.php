<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


    // /**
    //  * Where to redirect users after login.
    //  *
    //  * @var string
    //  */
    // protected $redirectTo = RouteServiceProvider::HOME;

    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function store()
    {
        return view('auth.login');
    }
    public function authenticate(Request $request)
    {
        // dd($request->all());
        $credentials = [
            'urs_email' => $request->urs_email,
            'password' => $request->password,
            'urs_status' => 1,
            ];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->hasAnyRole(['admin', 'Super Admin'])) {
                return redirect()->route('admin_home')->with('success_mess', 'Đăng nhập thành công!');
            } else {
                return redirect()->route('dashboard', $user->id)->with('success_mess', 'Đăng nhập thành công!');
            }
        } else {
            return redirect()->back()->with('error_mess', "Tài khoản bị khoá hoặc sai tài khoản hoặc mật khẩu !");
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home')->with('success_mess', 'Đăng xuất thành công!');
    }
}