<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreatUserRequest;
use App\Providers\RouteServiceProvider;
use App\Repositories\User\UserInterface;

use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\Object_ as TypesObject_;
use PhpParser\Node\Expr\Cast\Object_;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    use RegistersUsers;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $userInterface;
    protected $data;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserInterface $userInterface)
    {
        $this->middleware('guest');
        $this->userInterface = $userInterface;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function store()
    {
        return view('auth.register');
    }
    public function register(CreatUserRequest $request)
    {

        $data = [];
        $data['urs_name'] =  $request->urs_name;
        $data['urs_phone'] =  $request->urs_phone;
        $data['urs_email'] =  $request->urs_email;
        $data['password'] =  Hash::make($request->password);

        
        $createUser = $this->userInterface->saveUser($data);
        if ($createUser) 
        {
            return redirect()->route('login')->with('success_mess', 'Đăng ký thành công!');

        }else {
            return redirect()->back()->with('error_mess', 'Đăng ký không thành công!');
        }
    }
}
