<?php

namespace App\Http\Controllers;

use App\Repositories\Event\EventInterface;
use App\Repositories\Post\PostInterface;
use App\Repositories\Slider\SliderInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    protected $sliderInterface;
    protected $postInterface;
    protected $eventInterface;

    public function __construct(SliderInterface $sliderInterfaces, PostInterface $postInterface, EventInterface $eventInterface)
    {
        $this->sliderInterface = $sliderInterfaces;
        $this->postInterface = $postInterface;
        $this->eventInterface = $eventInterface;
    }
   
    public function index()
    {
        
        return view('home');
    }
    public function home()
    {
        $top = [
            'position_id' => 1,
            'sli_status' => 1
        ];
        $semitop = [
            'position_id' => 3,
            'sli_status' => 1
        ];
        $doitac = [
            'position_id' => 4,
            'sli_status' => 1
        ];
        $sliderTop = $this->sliderInterface->getAllByCondition($top);
        $sliderSemiTop = $this->sliderInterface->getAllByCondition($semitop);
        $sliderDoitac = $this->sliderInterface->getAllByCondition($doitac);

        $post = $this->postInterface->getAmountPostNewest(6);
       
        $eventSapdienra = $this->eventInterface->getEventComingOutSoon();
        // dd($eventSapdienra);
        $eventDangdienra = $this->eventInterface->getEventGoingOn();
        $eventNewCreat = $this->eventInterface->getEventNewCreat(8);
        $eventFree = $this->eventInterface->getEventFree();
        return view('client.home.home', compact('sliderTop', 'sliderSemiTop', 'sliderDoitac', 'post', 'eventSapdienra', 'eventDangdienra', 'eventNewCreat', 'eventFree'));
    }
    public function event()
    {
        return view('client.event.event');
    }
    public function register()
    {
        return view('client.home.register');
    }
    public function login()
    {
        return view('client.home.login');
    }
   
    

}
