<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddOrderRequest;
use App\Repositories\Order\OrderInterface;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $orderInterface;
    public function __construct(OrderInterface $orderInterface)
    {
        $this->orderInterface = $orderInterface;
    }
    public function addOrder(AddOrderRequest $request)
    {

        $param = [
            'ord_name' => $request->ord_name,
            'ord_email' => $request->ord_email,
            'ord_phone' => $request->ord_phone,
            'event_id' => $request->event_id,
            'ord_status' => 0
        ];
        $result = $this->orderInterface->create($param);
        if ($result) {
            return response()->json([
                'type' => 1,
                'mess' => 'Gửi thông tin thành công!'
            ]);
        } else {
            return response()->json([
                'type' => 2,
                'mess' => 'Có lỗi sảy ra, vui lòng thử lại!'
            ]);
        }
    }
}