<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;

class Rating extends Model
{
    protected $fillable = [
        'rat_title', 'rat_star', 'rat_content', 'event_id', 'rat_status'
    ];
    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }
}
