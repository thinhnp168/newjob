<?php

namespace App\Handlers;

class LfmConfigHandler extends \UniSharp\LaravelFilemanager\Handlers\ConfigHandler
{
    public function userField()
    {
        if (auth()->user()->hasRole('admin|Super Admin')) {
            return '';
        } else {
            return auth()->user()->id;
        }
    }
}
