<?php

namespace App\Repositories\Position;

use App\Repositories\BaseRepository;
use App\Position;
use App\Repositories\Position\PositionInterface;

class PositionRepository extends BaseRepository implements PositionInterface
{

    public function getModel()
    {
        return Position::class;
    }
}