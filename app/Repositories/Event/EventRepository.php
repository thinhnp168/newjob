<?php

namespace App\Repositories\Event;

use App\Repositories\BaseRepository;
use App\Event;
use App\Repositories\Event\EventInterface;
use Carbon\Carbon;

class EventRepository extends BaseRepository implements EventInterface
{

    public function getModel()
    {
        return Event::class;
    }
    public function getEventComingOutSoon()
    {
        $now = Carbon::now();
        $nowDate = $now->toDateString();
        $ngaycuoisapdienra = $now->addDays(20);
        $event = $this->model->where('eve_start_date', '<', $ngaycuoisapdienra)
        ->where('eve_start_date', '>', $nowDate)
        ->where('eve_status', 1)
        ->get();
        return $event;
    }
    public function getEventGoingOn()
    {
        $now = Carbon::now();
        $nowDate = $now->toDateString();
        $event = $this->model->where('eve_start_date', '<', $nowDate)
        ->where('eve_end_date', '>', $nowDate)
        ->where('eve_status', 1)
        ->get();
        return $event;
    }
    public function getEventNewCreat($limit)
    {
        $event = $this->model
        ->orderBy('created_at', 'desc')->limit($limit)
        ->where('eve_status', 1)
        ->get();
        return $event;
    }
    public function getEventFree()
    {
        $event = $this->model
        ->where('eve_price', 0)
        ->where('eve_status', 1)
        ->get();
        return $event;
    }
    public function getAmountEventRandom($amount)
    {
        return  $this->model->where('eve_status', 1)->inRandomOrder()->limit($amount)->get();
        
    }
}