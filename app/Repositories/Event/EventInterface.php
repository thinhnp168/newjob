<?php

namespace App\Repositories\Event;

interface EventInterface
{
    public function create($request = []);
    public function getAll();
    public function getAllConditionWith($attributes = [],$relation=[]);
    public function getIdConditionWith($id, $relation=[]);
    public function changeInfo($id, $dataRequest = []);
    public function changeValue($id, $dataRequest = []);
    public function findOrFail($id);
    public function update($id, $attributes = []);
    public function delete($id);
    public function getEventComingOutSoon();
    public function getEventGoingOn();
    public function getEventNewCreat($limit);
    public function getEventFree();
    public function getAmountEventRandom($amount);
}