<?php

namespace App\Repositories\User;

interface UserInterface {
    
    public function saveUser($dataRequest = []);
    public function getAll();
    public function changeInfo($id, $dataRequest = []);
    public function findOrFail($id);
    public function update($id, $attributes = []);
    public function delete($id);
}