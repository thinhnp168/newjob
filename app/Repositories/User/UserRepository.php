<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;
use App\Repositories\User\UserInterface;
use App\User;

class UserRepository extends BaseRepository implements UserInterface
{

    public function getModel()
    {
        return User::class;
    }
   
    public function saveUser($dataRequest = []) {
        try {
            $this->create($dataRequest);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
        
    }
}
