<?php

namespace App\Repositories\Config;

use App\Repositories\BaseRepository;
use App\Config;
use App\Repositories\Config\ConfigInterface;

class ConfigRepository extends BaseRepository implements ConfigInterface
{

    public function getModel()
    {
        return Config::class;
    }
}