<?php

namespace App\Repositories\Order;

use App\Repositories\BaseRepository;
use App\Order;
use App\Repositories\Order\OrderInterface;

class OrderRepository extends BaseRepository implements OrderInterface
{

    public function getModel()
    {
        return Order::class;
    }
}