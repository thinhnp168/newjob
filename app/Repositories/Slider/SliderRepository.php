<?php

namespace App\Repositories\Slider;

use App\Repositories\BaseRepository;
use App\Slider;
use App\Repositories\Slider\SliderInterface;

class SliderRepository extends BaseRepository implements SliderInterface
{

    public function getModel()
    {
        return Slider::class;
    }
}