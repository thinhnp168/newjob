<?php

namespace App\Repositories;

interface RepositoryInterface
{
    // /**
    //  * Get all
    //  * @return mixed
    //  */
    public function getAll();

    // /**
    //  * Get one
    //  * @param $id
    //  * @return mixed
    //  */
    public function findOrFail($id);

    // /**
    //  * Create
    //  * @param array $attributes
    //  * @return mixed
    //  */
    public function create($attributes = []);
    public function changeInfo($id, $attributes = []);

    // /**
    //  * Update
    //  * @param $id
    //  * @param array $attributes
    //  * @return mixed
    //  */
    public function update($id, $attributes = []);

    // /**
    //  * Delete
    //  * @param $id
    //  * @return mixed
    //  */
    // public function delete($id);

    /**
     * Get one condition
     * @param array $value
     * @return mixed
     */
    // public function findBy($value);
    // public function getAllConditionWith($attributes = [],$relation=[]);

    //  /**
    //  * Get paginate condition
    //  * @param array $perpage
    //  * @return mixed
    //  */
    public function paginate(int $perPage, array $relation = []);
}