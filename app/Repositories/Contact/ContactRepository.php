<?php

namespace App\Repositories\Contact;

use App\Repositories\BaseRepository;
use App\Contact;
use App\Repositories\Contact\ContactInterface;

class ContactRepository extends BaseRepository implements ContactInterface
{

    public function getModel()
    {
        return Contact::class;
    }
}