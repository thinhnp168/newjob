<?php

namespace App\Repositories\Post;

interface PostInterface
{
    public function create($request = []);
    public function getAll();
    public function getAllConditionWith($attributes = [],$relation=[]);
    public function getIdConditionWith($id, $relative); 
    public function changeInfo($id, $dataRequest = []);
    public function findOrFail($id);
    public function update($id, $attributes = []);
    public function delete($id);
    public function getAmountPostNewest($amount);
    public function getAmountPostRandom($amount);
    public function paginate(int $perPage, array $relation = []);
}