<?php

namespace App\Repositories\Post;

use App\Repositories\BaseRepository;
use App\Post;
use App\Repositories\Post\PostInterface;

class PostRepository extends BaseRepository implements PostInterface
{

    public function getModel()
    {
        return Post::class;
    }
    public function getAmountPostNewest($amount)
    {
        return  $this->model->where('pts_status', 1)->orderBy('created_at', 'desc')->limit($amount)->get();
        
    }
    public function getAmountPostRandom($amount)
    {
        return  $this->model->where('pts_status', 1)->inRandomOrder()->limit($amount)->get();
        
    }
}