<?php

namespace App\Repositories\Comment;

interface CommentInterface
{
    public function create($request = []);
    public function getAll();
    public function getAllConditionWith($attributes = [], $relation = []);
    public function changeInfo($id, $dataRequest = []);
    public function changeValue($id, $dataRequest = []);
    public function findOrFail($id);
    public function update($id, $attributes = []);
    public function delete($id);
}