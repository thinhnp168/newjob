<?php

namespace App\Repositories\Comment;

use App\Repositories\BaseRepository;
use App\Comment;
use App\Repositories\Comment\CommentInterface;

class CommentRepository extends BaseRepository implements CommentInterface
{

    public function getModel()
    {
        return Comment::class;
    }
}