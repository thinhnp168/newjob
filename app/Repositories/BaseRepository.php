<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    //model muốn tương tác
    protected $model;

   //khởi tạo
    public function __construct()
    {
        $this->setModel();
    }

    //lấy model tương ứng
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }
    
    public function getAll()
    {
        return $this->model->all();
    }

    public function getIdConditionWith($id,$relation=[])
    {
        return $this->model->where('id', $id)->with($relation)->first();
    }
    public function getAllConditionWith($attributes = [],$relation=[])
    {
        return $this->model->where($attributes)->with($relation)->get();
    }
    // public function getAllConditionWithCount($attributes = [],$relation=[])
    // {
    //     return $this->model->where($attributes)->withCount($relation)->get();
    // }

    public function getAllByCondition($attributes = [])
    {
        return $this->model->where($attributes)->get();
    }
    
    public function findOrFail($id)
    {
        $result = $this->model->findOrFail($id);

        return $result;
    }
    
    public function create($attributes = [])
    {
        try {
            $result = $this->model->create($attributes);
            return $result;
        } catch (\Throwable $th) {
            return false;
        }
    }
    
    public function changeInfo ($id, $request = []) 
    {
        $item = $this->model->findOrFail($id);
        if(!empty($item)) {
            $type = $request['type'];
            if ($item->$type == 1) {
                $item->$type = 0;
            } else {
                $item->$type = 1;
            }
            $result = $item->save();
            if ($result) {
                return $item;
            } else {
                return false;
            }
        }else {
            return false;
        }
        
        
    }
    public function changeValue ($id, $request = []) 
    {
        $item = $this->model->findOrFail($id);
        if(!empty($item)) {
            $type = $request['type'];
            $attributes = [
                $type => $request['value'],
            ];
            $result = $item->update($attributes);
            if ($result) {
                return true;
            } else {
                return false;
            }
        }else {
            return false;
        }
        
        
    }
    public function update($id, $attributes = [])
    {
        $result = $this->findOrFail($id);
        if ($result) {
            try {
                $result->update($attributes);
                return $result;
            } catch (\Throwable $th) {
                return false;
            }
        }

        return false;
    }
    
    public function delete($id)
    {
        $result = $this->findOrFail($id);
        if ($result) {
            try {
                $result->delete();
                return true;
            } catch (\Throwable $th) {
                return false;
            }
            
        }

        return false;
    }
    public function paginate(int $perPage, array $relation = [])
    {
        return $this->model->with($relation)->orderBy('id', 'DESC')->paginate($perPage);
    }
    /**
     * @param mixed $value
     * @return mixed|Model|static
     */
    public function encryptPassword($password){
        $ciphering = "AES-128-CTR";
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        $encryption_iv = '1356421498701236';
        $encryption_key = env('TOKEN_KEY');
        dd($encryption_key);
        return openssl_encrypt($password, $ciphering,$encryption_key, $options, $encryption_iv);
    }

    public function decryptPassword($password){
        $ciphering = "AES-128-CTR";
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        $encryption_iv = '1356421498701236';
        $encryption_key = env('TOKEN_KEY');
        return openssl_decrypt($password, $ciphering,$encryption_key, $options, $encryption_iv);
    }
}