<?php

namespace App\Repositories\Rating;

use App\Repositories\BaseRepository;
use App\Rating;
use App\Repositories\Rating\RatingInterface;

class RatingRepository extends BaseRepository implements RatingInterface
{

    public function getModel()
    {
        return Rating::class;
    }
}