<?php

namespace App\Repositories\Phone;

use App\Repositories\BaseRepository;
use App\Phone;
use App\Repositories\Phone\PhoneInterface;

class PhoneRepository extends BaseRepository implements PhoneInterface
{

    public function getModel()
    {
        return Phone::class;
    }
}