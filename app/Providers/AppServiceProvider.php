<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\BaseRepository;
use App\Repositories\Config\ConfigInterface;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */

    protected $configInterface;

    public function register()
    {
        $this->app->singleton(
            \App\Repositories\RepositoryInterface::class,
            \App\Repositories\BaseRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\User\UserInterface::class,
            \App\Repositories\User\UserRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Post\PostInterface::class,
            \App\Repositories\Post\PostRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Event\EventInterface::class,
            \App\Repositories\Event\EventRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Contact\ContactInterface::class,
            \App\Repositories\Contact\ContactRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Phone\PhoneInterface::class,
            \App\Repositories\Phone\PhoneRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Comment\CommentInterface::class,
            \App\Repositories\Comment\CommentRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Rating\RatingInterface::class,
            \App\Repositories\Rating\RatingRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Config\ConfigInterface::class,
            \App\Repositories\Config\ConfigRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Slider\SliderInterface::class,
            \App\Repositories\Slider\SliderRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Position\PositionInterface::class,
            \App\Repositories\Position\PositionRepository::class,
        );
        $this->app->singleton(
            \App\Repositories\Order\OrderInterface::class,
            \App\Repositories\Order\OrderRepository::class,
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            $this->configInterface = resolve(\App\Repositories\Config\ConfigInterface::class);
            $config = $this->configInterface->getIdConditionWith(1, []);
            View::share('CONFIG_VIEW_SHARE', $config);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}