<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Comment;
class Post extends Model
{
    protected $fillable = [
        'pts_name', 'pts_image','pts_seo_title', 'pts_description', 'pts_seo_description', 'pts_seo_keyword','pts_slug', 'user_id', 'pts_content','pts_status'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function comment()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }
}
