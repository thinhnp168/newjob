<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
        'con_name', 
        'con_website', 
        'con_email', 
        'con_tagline', 
        'con_facebook', 
        'con_youtube', 
        'con_zalo', 
        'con_adress_hn', 
        'con_adress_hcm', 
        'con_adress', 
        'con_phone_hn', 
        'con_phone_hcm', 
        'con_map', 
        'con_logo_top', 
        'con_logo_top_small', 
        'con_seo_image', 
        'con_favicon', 
        'con_logo_bot', 
        'con_seo_title', 
        'con_seo_description' , 
        'con_seo_keyword'
    ];
}
