<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Rating;

class Event extends Model
{
    protected $fillable = [
        'eve_name', 'eve_slug', 'eve_image', 'eve_avatar', 'eve_description', 'eve_start_date', 'eve_end_date', 'eve_end_hour', 'eve_start_hour', 'eve_price', 'eve_address_type', 'eve_address', 'eve_love', 'eve_introduce', 'eve_content', 'eve_lecturer', 'eve_is_hot', 'eve_status', 'eve_seo_title', 'eve_seo_description', 'eve_seo_keyword', 'user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function rating()
    {
        return $this->hasMany(Rating::class, 'event_id', 'id');
    }
}
