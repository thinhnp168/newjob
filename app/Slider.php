<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Position;

class Slider extends Model
{
    protected $fillable = [
        'sli_title', 'sli_image','sli_link', 'sli_content', 'position_id', 'sli_status'
    ];
    public function position()
    {
        return $this->belongsTo(Position::class, 'position_id', 'id');
    }
}
