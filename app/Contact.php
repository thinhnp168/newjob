<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'cct_name', 'cct_title', 'cct_email', 'cct_content', 'cct_status'
    ];
}
