<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Post;

class Comment extends Model
{
    protected $fillable = [
        'cmt_name', 'cmt_email', 'cmt_content', 'cmt_status', 'user_id', 'post_id', 'parent_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function post()
    {
        return $this->belongsTo(Post::class, 'user_id', 'id');
    }

}
