<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'ord_name', 'ord_email', 'ord_phone', 'ord_content', 'event_id'
    ];
}